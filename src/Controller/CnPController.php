<?php
namespace Drupal\click_pledge_connect\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \SoapClient;
use SimpleXMLElement;

class CnPController extends ControllerBase
{
    const CFCNP_PLUGIN_UID = "14059359-D8E8-41C3-B628-E7E030537905";
    const CFCNP_PLUGIN_SKY = "5DC1B75A-7EFA-4C01-BDCD-E02C536313A3";
    public function ajaxCallback($variable1,$variable2)
    {
        $connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
	$client   = new SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
        if(isset($variable1) && isset($variable2) && $variable1!="" && $variable2!="")
        {
            $accountid = $variable1;
            $accountguid = $variable2;
            $xmlr = new SimpleXMLElement("<GetAccountDetail></GetAccountDetail>");
            $xmlr->addChild('accountId', $accountid);
            $xmlr->addChild('accountGUID', $accountguid);
            $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
            $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
            $response = $client->GetAccountDetail($xmlr); 

            $responsearr =  $response->GetAccountDetailResult->AccountNickName;
           
            return new JsonResponse(array("status"=>$responsearr));
	 
        }
        else
        {
            return new JsonResponse(array("status"=>false));
        }
    }
    public function getAllCampaigns($variable)
    {
        $str=explode("@", $variable);
	$connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
	$client   = new SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
        $accountguid=$str[1];
        $accountid=$str[0];
        
        $xmlr  = new SimpleXMLElement("<GetActiveCampaignList2></GetActiveCampaignList2>");
	$xmlr->addChild('accountId', $accountid);
	$xmlr->addChild('AccountGUID', $accountguid);
	$xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
	$xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
        
        $response = $client->GetActiveCampaignList2($xmlr); 
        $responsearr =  $response->GetActiveCampaignList2Result->connectCampaign;
       
        //print_r($responsearr);
        $camrtrnval = "<option value=''>Select Campaign Name</option>";
        /*if(count($responsearr) == 1)
            {
             $camrtrnval.= "<option value='".$responsearr->alias."'>".$responsearr->name."</option>";
            }else{
            for($inc = 0 ; $inc < count($responsearr);$inc++)
            {
             $camrtrnval .= "<option value='".$responsearr[$inc]->alias."'>".$responsearr[$inc]->name."</option>";
            }

	 }*/
        $orderRes=array();
        foreach($responsearr as $obj)
        {
            $orderRes[$obj->alias]=$obj->name;
        }
        natcasesort($orderRes);
        $camrtrnval = "<option value=''>Select Campaign Name</option>";
        if(count($orderRes)>0)
        {
            foreach($orderRes as $key=>$value)
            {
             $camrtrnval .= "<option value='".$key."'>".$value."</option>";
            }
	 }
         //echo $camrtrnval;
        return new JsonResponse(array("data"=>$camrtrnval));
    }
    public function getActivityForms($variable)
    {
        $str=explode("@", $variable);
        $connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
	$client   = new SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
        $cnpaccountID      = $str[0];
        $cnpaccountguidID  =$str[1];
        $cnpcampaignId     = $str[2];
        $xmlr  = new SimpleXMLElement("<GetActiveFormList2></GetActiveFormList2>");
        $xmlr->addChild('accountId', $cnpaccountID);
        $xmlr->addChild('AccountGUID', $cnpaccountguidID);
        $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
        $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
        $xmlr->addChild('campaignAlias', $cnpcampaignId);

        $frmresponse    =  $client->GetActiveFormList2($xmlr); 	
        $frmresponsearr =  $frmresponse->GetActiveFormList2Result->form;	
        /*$rtrnval = "<option value=''>Select Form Name</option>";
        if(count($frmresponsearr) == 1)
        {
            $rtrnval.= "<option value='".$frmresponsearr->formGUID."'>".$frmresponsearr->formName."</option>";
        }else{
        for($finc = 0 ; $finc < count($frmresponsearr);$finc++)
        {
            $rtrnval.= "<option value='".$frmresponsearr[$finc]->formGUID."'>".$frmresponsearr[$finc]->formName."</option>";
        }

	}*/
        $orderRes=array();
        foreach($frmresponsearr as $obj)
        {
            $orderRes[$obj->formGUID]=$obj->formName;
        }
        natcasesort($orderRes);
        $rtrnval = "<option value=''>Select Form Name</option>";
        if(count($orderRes)>0)
        {
            foreach($orderRes as $key=>$value)
            {
                $rtrnval .= "<option value='".$key."'>".$value."</option>";
            }
	 }
        return new JsonResponse(array("data"=>$rtrnval));
        //return new JsonResponse(array("Welcome"));
    }
    public function saveFormData(){
        $data = $_POST;
        $module_handler = \Drupal::service('module_handler');
        $module_path = $module_handler->getModule('click_pledge_connect')->getPath();
        
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        
        if(isset($_FILES))
        {
            $filename=$_FILES['file']['name'];
            $tname=$_FILES['file']['tmp_name'];
            move_uploaded_file($tname, $module_path."/banners/$filename");
        }
        else
        {
            if($data['cnpc_action']=="edit")
            {
                if($data['file'])
                {
                    $filename = $data['file'];
                }
                else
                {
                    $filename="";
                }
            }
            else
            {
                $filename="";
            }
        }
        
        $cnpc_account_str=explode("||",$data['cnpc_account']);
        $guid=$cnpc_account_str[1];
        $accid=$cnpc_account_str[0];
        $refId=$cnpc_account_str[2];
        
        $display_type=$data['cnpc_display_type'];
        $link_type=(isset($data['link_type']))?$data['link_type']:"";
        $button_label=(isset($data['button_label']))?$data['button_label']:"";
        if($data['cnpc_end_datetime'])
        {
            $cnpc_end_datetime=date("Y-m-d h:i:s",strtotime($data['cnpc_end_datetime']));
        }
        else
        {
            $cnpc_end_datetime="0000-00-00 00:00:00";
        }
        
        $cnpc_group_name=trim($data['cnpc_group_name']);
        $cnpc_group_name_shortcode=str_replace(' ', '-', $cnpc_group_name);
        $cnpc_message=$data['cnpc_message'];
        $cnpc_start_datetime=date("Y-m-d h:i:s",strtotime($data['cnpc_start_datetime']));
        $cnpc_status=$data['cnpc_status'];
        $form_name="Test";
        $shortcode="[cnpform]".$cnpc_group_name_shortcode."[/cnpform]";
        $table_name1 = $prefix.'dp_cnpc_forminfo';
        $iID=array();
       if($data['cnpc_action']=="add")
       {
        $query1 = \Drupal::database()->insert($table_name1)
	->fields([
		'cnpform_groupname',
		'cnpform_cnpstngs_ID',
		'cnpform_AccountNumber',
		'cnpform_guid',
		'cnpform_type',
		'cnpform_ptype',
		'cnpform_text',
		'cnpform_img',
		'cnpform_shortcode',
		'cnpform_custommsg',
		'cnpform_Form_StartDate',
		'cnpform_Form_EndDate',
		'cnpform_status',
	])
	->values(array(
                $cnpc_group_name,
		$refId,
		$accid,
		$guid,
		$display_type,
		 $link_type,
		$button_label,
		$filename,
		$shortcode,
		$cnpc_message,
		$cnpc_start_datetime,
		 $cnpc_end_datetime,
		$cnpc_status,
	))
	->execute();
       }
       else
       {
        
        $num_updated = $connection->update($table_name1)
        ->fields([
          'cnpform_groupname' => $cnpc_group_name,
          //'cnpform_cnpstngs_ID' => $data['cnpc_edit_id'],
          'cnpform_AccountNumber' => $accid,
          'cnpform_guid' => $guid,
          'cnpform_type' => $display_type,
          'cnpform_ptype' => $link_type,
          'cnpform_text' => $button_label,
          'cnpform_img' => $filename,
          'cnpform_shortcode' => $shortcode,
          'cnpform_custommsg' => $cnpc_message,
          'cnpform_Form_StartDate' => $cnpc_start_datetime,
          'cnpform_Form_EndDate' => $cnpc_end_datetime,
          'cnpform_status' => $cnpc_status,
        ])
        ->condition('cnpform_GID', $data['cnpc_edit_id'])
        ->execute();
        
       }
        
        $table_name = $prefix.'dp_cnpc_formsdtl';
        if($data['cnpc_action']=="edit")
        {
            $nid = $data['cnpc_edit_id'];
            
            $did=$data['cnpc_edit_id'];
            $status = $connection->delete($table_name)
            ->condition('cnpform_cnpform_ID', $did)
            ->execute();
            
        }
        else
        {
            $nid=$query1;
        }
        
        //Insert form group information
        $ls=explode("###",$data['cnpc_camp_list'],-1);
        $fs=explode("###",$data['cnpc_form_list'],-1);
        $gu=explode("###",$data['formguid'],-1);
        $sd=explode("###",$data['startdate'],-1);
        $ed=explode("###",$data['enddate'],-1);
        $pl=explode("###",$data['params_list'],-1);
        
        $form_list_titles=explode("###",$data['form_list_titles'],-1);        
   
         $iID=array();
        for($i=0;$i<count($ls);$i++)
        {
            $cnamp_list=$ls[$i];
            $form_list = $fs[$i];
            $guid_list = $gu[$i];
            if($pl[$i])
            {
                $param_list = $pl[$i];
            }
            else
            {
                $param_list="";
            }
            $sd_list = date("Y-m-d h:i:s",strtotime($sd[$i]));
            if($ed[$i])
            {
                $ed_list = date("Y-m-d h:i:s",strtotime($ed[$i]));
            }
            else
            {
                $ed_list="0000-00-00 00:00:00";
            }
            $flt = $form_list_titles[$i];
            
            $query = \Drupal::database()->insert($table_name)
            ->fields([
                    'cnpform_cnpform_ID',
                    'cnpform_CampaignName',
                    'cnpform_FormName',
                    'cnpform_GUID',
                    'cnpform_FormStartDate',
                    'cnpform_FormEndDate',
                    'cnpform_FormStatus',
                    'cnpform_urlparameters'
            ])
            ->values(array(
                    $nid,
                    $cnamp_list,
                    $flt,
                    $guid_list,
                    $sd_list,
                   $ed_list,
                   $cnpc_status,
                   $param_list,
            ))
            ->execute();
            $iID[]=$query;
        }
        
        
        if(count($iID)>0)
        {
            return new JsonResponse(array("data"=>"success"));
        }
        else
        {
            return new JsonResponse(array("data"=>"fail"));
        }
    }
    public function updateFormStatus($variableone,$variabletwo)
    {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name1 = $prefix.'dp_cnpc_forminfo';
        $num_updated = $connection->update($table_name1)
        ->fields([
          'cnpform_status' => $variableone,
        ])
        ->condition('cnpform_GID', $variabletwo)
        ->execute();
        if($num_updated)
        {
            return new JsonResponse(array("data"=>"success"));
        }
        else
        {
            return new JsonResponse(array("data"=>"fail"));
        }
    }
    public function deleteFormDetails($variable)
    {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_formsdtl';
        $num_deleted = $connection->delete($table_name)
        ->condition('cnpform_id', $variable)
        ->execute();
        return new JsonResponse(array($num_deleted));
    }
    public function deleteFormGroup($variable)
    {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_forminfo';
        
        $deleted_info = $connection->delete($table_name)
        ->condition('cnpform_GID', $variable)
        ->execute();
        if($deleted_info)
        {
            $table_name1 = $prefix.'dp_cnpc_formsdtl';
            $deleted_dtl = $connection->delete($table_name1)
            ->condition('cnpform_cnpform_ID', $variable)
            ->execute();
            $status="success";
        }
        else
        {
            $status="fail";
        }
        return new JsonResponse(array("data"=>$status));
    }
    function cnpcRedirect($path) { 
        $response = new RedirectResponse($path, 302);
        $response->send();
        return;
    }
    public function deleteSettingsDetails($variable)
    {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpcsettingsdtl';
        
        $checkTable = $prefix.'dp_cnpc_forminfo';
        $sql = "SELECT * FROM " .$checkTable." where cnpform_cnpstngs_ID=".$variable;
        $query = $connection->query($sql);
        if(count($query->fetchAll())==0)
        {
            //return new JsonResponse(array("data"=>"success"));
            $deleted_dt1 = $connection->delete($table_name)
            ->condition('cnpstngs_ID', $variable)
            ->execute();
            if($deleted_dt1)
            {
                return new JsonResponse(array("data"=>"success"));
            }
            else
            {
                return new JsonResponse(array("data"=>"fail"));
            }
            
        }
        else
        {
            return new JsonResponse(array("data"=>"fail"));
        }
    }
    public function refreshAccounts(){
        $accid = $_POST['saccid'];
        $guid = $_POST['sguid'];
        $recid = $_POST['srecid'];
        $connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
	$client   = new SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
        if(isset( $accid) && isset($guid) && $accid!="" && $guid!="")
        {
            $accountid = $accid;
            $accountguid = $guid;
            $xmlr = new SimpleXMLElement("<GetAccountDetail></GetAccountDetail>");
            $xmlr->addChild('accountId', $accountid);
            $xmlr->addChild('accountGUID', $accountguid);
            $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
            $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
            $response = $client->GetAccountDetail($xmlr); 
            $responsearr =  $response->GetAccountDetailResult->AccountNickName;
            
            $connection= \Drupal::database();
            $prefix=$connection->tablePrefix();
            $table_name = $prefix.'dp_cnpcsettingsdtl';
           
            $updated = $connection->update($table_name)
            ->fields([
              'cnpstngs_frndlyname' => $responsearr,
            ])
            ->condition('cnpstngs_ID', $recid)
            ->execute();
            if($updated)
            {
                return new JsonResponse(array("status"=>"success"));
            }
            else
            {
                return new JsonResponse(array("status"=>"fail"));
            }
	 
        }
        else
        {
            return new JsonResponse(array("status"=>"fail"));
        }
        
    }
    public function saveChannelData(){
        $data = $_POST;
   
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        if($data['cnpc_action']!="edit")
        {
        $cnpc_account_str=explode("||",$data['cnpc_account']);
        $guid=$cnpc_account_str[1];
        $accid=$cnpc_account_str[0];
        $refId=$cnpc_account_str[2];
        }
        if($data['cnpc_end_datetime'])
        {
            $cnpc_end_datetime=date("Y-m-d h:i:s",strtotime($data['cnpc_end_datetime']));
        }
        else
        {
            $cnpc_end_datetime="0000-00-00 00:00:00";
        }
       
        $chnlcnpc_group_name=trim($data['cnpc_group_name']);
        $chnlcnpc_group_name_shortcode=str_replace(' ', '-', $chnlcnpc_group_name);
        $cnpc_message=$data['cnpc_message'];
        $cnpc_start_datetime=date("Y-m-d h:i:s",strtotime($data['cnpc_start_datetime']));
        $cnpc_status=$data['cnpc_status'];
       
        $shortcode="[cnppledgetv]".$chnlcnpc_group_name_shortcode."[/cnppledgetv]";
        $table_name1 = $prefix.'dp_cnpc_channelgrp';
        $ciID=array();
       if($data['cnpc_action']=="add")
       {
        $query1 = \Drupal::database()->insert($table_name1)
    ->fields([
        'cnpchannelgrp_groupname',
        'cnpchannelgrp_cnpstngs_ID',
        'cnpchannelgrp_shortcode',
        'cnpchannelgrp_custommsg',
        'cnpchannelgrp_channel_StartDate',
        'cnpchannelgrp_channel_EndDate',
        'cnpchannelgrp_status',
    ])
    ->values(array(
                $chnlcnpc_group_name,
                $refId,
                $shortcode,
                $cnpc_message,
                $cnpc_start_datetime,
                 $cnpc_end_datetime,
                $cnpc_status,
    ))
    ->execute();
       }
       else
       {
         $table_name1 = $prefix.'dp_cnpc_channelgrp';
        $num_updated =  \Drupal::database()->update($table_name1)
        ->fields([
          'cnpchannelgrp_groupname' => $chnlcnpc_group_name,
          'cnpchannelgrp_shortcode' => $shortcode,
          'cnpchannelgrp_custommsg' => $cnpc_message,
          'cnpchannelgrp_channel_StartDate' => $cnpc_start_datetime,
          'cnpchannelgrp_channel_EndDate' => $cnpc_end_datetime,
          'cnpchannelgrp_status' => $cnpc_status,
        ])
        ->condition('cnpchannelgrp_ID', $data['cnpc_edit_id'])
        ->execute();
       
       }
       
        $table_name = $prefix.'dp_cnpc_channeldtl';
        if($data['cnpc_action']=="edit")
        {
            $nid = $data['cnpc_edit_id'];
           
            $did=$data['cnpc_edit_id'];
            $status =  \Drupal::database()->delete($table_name)
            ->condition('cnpchannel_cnpchannelgrp_ID', $did)
            ->execute();
           
        }
        else
        {
            $nid=$query1;
        }
       
        //Insert Channel group information
        $ls=explode("###",$data['cnpc_chnl_list'],-1);
        $sd=explode("###",$data['chnlstartdate'],-1);
        $ed=explode("###",$data['chnlenddate'],-1);
            
  
         $ciID=array();
        for($i=0;$i<count($ls);$i++)
        {
            $cnamp_list=$ls[$i];
            $form_list = $fs[$i];
            $guid_list = $gu[$i];
            $sd_list = date("Y-m-d h:i:s",strtotime($sd[$i]));
            if($ed[$i])
            {
                $ed_list = date("Y-m-d h:i:s",strtotime($ed[$i]));
            }
            else
            {
                $ed_list="0000-00-00 00:00:00";
            }
            $flt = $form_list_titles[$i];
           
            $cquery = \Drupal::database()->insert($table_name)
            ->fields([
                    'cnpchannel_cnpchannelgrp_ID',
                    'cnpchannel_channelName',
                    'cnpchannel_channelStartDate',
                    'cnpchannel_channelEndDate',
                    'cnpchannel_channelStatus',
            ])
            ->values(array(
                    $nid,
                    $cnamp_list,
                    $sd_list,
                    $ed_list,
                    $cnpc_status,
            ))
            ->execute();
            $ciID[]=$cquery;
        }
       
     
        if(count($ciID)>0)
        {
            return new JsonResponse(array("data"=>"success"));
        }
        else
        {
            return new JsonResponse(array("data"=>"fail"));
        }
    }
    public function deletechannelDetails($variable)
    {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_channeldtl';
        $num_deleted = $connection->delete($table_name)
        ->condition('cnpchannel_id', $variable)
        ->execute();
        return new JsonResponse(array($num_deleted));
    }
    public function deletechannelGroup($variable)
    {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_channelgrp';
       
        $deleted_info = $connection->delete($table_name)
        ->condition('cnpchannelgrp_ID', $variable)
        ->execute();
        if($deleted_info)
        {
            $table_name1 = $prefix.'dp_cnpc_channeldtl';
            $deleted_dtl = $connection->delete($table_name1)
            ->condition('cnpchannel_cnpchannelgrp_ID', $variable)
            ->execute();
            $status="success";
        }
        else
        {
            $status="fail";
        }
        return new JsonResponse(array("data"=>$status));
    }
    public function getAllChannels($variable)
    {
        $str=explode("@", $variable);
	$connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
		
			 $module_handler = \Drupal::service('module_handler');
			 $host = \Drupal::request()->getSchemeAndHttpHost();
			 $module_path = $module_handler->getModule('click_pledge_connect')->getPath()."/Auth2.wsdl";
	$client   = new SoapClient($module_path , $connect);$camrtrnval="";
        $caccountguid=$str[1];
        $caccountid=$str[0];
    $xmlr  = new SimpleXMLElement("<GetPledgeTVChannelList></GetPledgeTVChannelList>");
	$xmlr->addChild('accountId', $caccountid);
	$xmlr->addChild('AccountGUID', $caccountguid);
	$xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
	$xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
        
      $response = $client->GetPledgeTVChannelList($xmlr);
       $responsearr =  $response->GetPledgeTVChannelListResult->PledgeTVChannel;
		//print_r($responsearr);
       /* $camrtrnval = "<option value=''>Select channels</option>";
        if(count($responsearr) == 1)
            { if($_REQUEST['slcamp'] == $responsearr->ChannelURLID){$displymsg ="selected"; }else{$displymsg ="";}
             $camrtrnval.= "<option value='".$responsearr->ChannelURLID."' $displymsg>".$responsearr->ChannelName." (".$responsearr->ChannelURLID.")</option>";
            }else{
            for($inc = 0 ; $inc < count($responsearr);$inc++)
            {
             if($_REQUEST['slcamp'] == $responsearr[$inc]->ChannelURLID){$displymsg = "selected"; }else{$displymsg ="";}
		 $camrtrnval .= "<option value='".$responsearr[$inc]->ChannelURLID."' $displymsg >".$responsearr[$inc]->ChannelName." (".$responsearr[$inc]->ChannelURLID.")</option>";
            }

	 }*/
       $orderRes=array();
        foreach($responsearr as $obj)
        {
            $orderRes[$obj->ChannelURLID]=$obj->ChannelName;
        }
        natcasesort($orderRes);
        $camrtrnval = "<option value=''>Select channels</option>";
        if(count($orderRes)>0)
        {
            foreach($orderRes as $key=>$value)
            {
             $camrtrnval .= "<option value='".$key."'>".$value."</option>";
            }
	 }
         //echo $camrtrnval;
        return new JsonResponse(array("data"=>$camrtrnval));
    }
    public function updatePTVFormStatus($variable1,$variable2)
    {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name1 = $prefix.'dp_cnpc_channelgrp';
        $num_updated = $connection->update($table_name1)
        ->fields([
          'cnpchannelgrp_status' => $variable1,
        ])
        ->condition('cnpchannelgrp_ID', $variable2)
        ->execute();
        if($num_updated)
        {
            return new JsonResponse(array("data"=>"success"));
        }
        else
        {
            return new JsonResponse(array("data"=>"fail"));
        }
    }
}
