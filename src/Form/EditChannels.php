<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface; 
use \SoapClient;
use SimpleXMLElement;

class EditChannels extends FormBase
{
    const CFCNP_PLUGIN_UID = "14059359-D8E8-41C3-B628-E7E030537905";
    const CFCNP_PLUGIN_SKY = "5DC1B75A-7EFA-4C01-BDCD-E02C536313A3";
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpc_edit_channel_group";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        //$config=$this->config("cnpcaccountform.settings");
        $form=$this->displayEditCnpcForm($form, $form_state);
        //return parent::buildForm($form, $form_state);
        return $form;
   }
   public function displayEditCnpcForm($form, $form_state)
   {
       //logo display
       date_default_timezone_set(drupal_get_user_timezone());
       
       
        $path = \Drupal::request()->getpathInfo();
        $arg  = explode('/',$path);
        $reqID=end($arg);
        $formInfo = $this->getChannelInfo($reqID);
        $formDetails = $this->getChannelDetails($reqID);
        //print_r($formInfo);
        $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a href="../cnp_form_help">Click & Pledge</a></li>'
            . '<li><a href="../cnp_form">Form Groups</a></li>'
            //. '<li><a href="../add_form_group">Add Form Group</a></li>'
            .'<li><a class="cnpccurrent" href="../cnppledgetvchannels">pledgeTV</a></li>'
            //.'<li><a href="../add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="../cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
               
        $html = '<a href="../cnppledgetvchannels" class="cnpc_button_link_edit">Go back to Channel Groups</a>';
        $form['gobackbtn_link_html'] = array(
            '#type'=> 'markup',
            '#markup'=> $html,
        );
        $form['cnpc_heading'] = array(
                '#prefix' => '<div class="cnp_heading1">',
                '#suffix' => '</div>',
        );
         $form['cnpc_channel_group_name'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Channel Group Name*'),  
            '#description' => $this->t('Please enter the channel name'),  
            '#default_value' =>$formInfo->cnpchannelgrp_groupname,
            '#attributes' => array("id"=>"cnpc_channel_group_name"),
        ];
        $data=$this->getRecords();
        if(count($data)>0)
        {
            if(count($data)==1)
            {
                $disabled=TRUE;
            }
            else
            {
                $disabled=FALSE;
            }
        }
        else
        {
            $disabled=false;
        }
        $options=array();
        foreach($data as $rec)
        {
            $options[$rec->cnpstngs_AccountNumber."||".$rec->cnpstngs_guid."||".$rec->cnpstngs_ID]=$rec->cnpstngs_frndlyname.' ( '.$rec->cnpstngs_AccountNumber.' )';
        }
 
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
        $form['cnpc_edit_id'] = [
            '#type' => 'hidden',
            '#default_value' => $reqID,
            '#attributes' => array("id"=>"cnpc_edit_id"),
        ];
        $form['cnpc_action'] = [
            '#type' => 'hidden',
            '#default_value' => 'edit',
            '#attributes' => array("id"=>"cnpc_action"),
        ];
        $form['cnpc_accounts'] = [  
            '#type' => 'select',  
            '#title' => $this->t('Account(s)*'),  
             '#options' => $options,
             '#default_value' => "",
            '#attributes' => array("id"=>"cnpc_accounts"),
            '#disabled' => $disabled  ,
             //"#default_value" => ,
        ]; 
         $form['cnpc_start_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Start Date & Time* [TimeZone: '.drupal_get_user_timezone().' ]'),  
            '#attributes' => array("id"=>"start_date_time"),
            '#default_value' =>date("F d, Y h:i:s a",strtotime($formInfo->cnpchannelgrp_channel_StartDate)),
            //'#default_value' => drupal_get_user_timezone(),
            //'#default_value' => date_default_timezone_get(),
        ]; 
         if($formInfo->cnpchannelgrp_channel_EndDate=="0000-00-00 00:00:00")
         {
             $ed="";
         }
         else
         {
             
             $ed = date("F d, Y h:i:s a",strtotime($formInfo->cnpchannelgrp_channel_EndDate));
         }
         $form['cnpc_end_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('End Date & Time'),  
            '#attributes' => array("id"=>"end_date_time"),
            '#default_value' =>$ed,
        ]; 
         
        
        
        $form['cnpc_message'] = [  
            '#type' => 'textarea',  
            '#title' => $this->t('No Valid Form Message'),  
            '#attributes' => array("id"=>"cnpc_message"),
            '#default_value' =>$formInfo->cnpchannelgrp_custommsg,
        ];
        $form['cnpc_status'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Status'),  
            '#options' => array("active"=>"Active","inactive"=>"Inactive"),
           '#attributes' => array("id"=>"cnpc_status"),
            '#default_value' =>$formInfo->cnpchannelgrp_status,
        ]; 
        
        //hidden form Starts here
       
         $noofFoms=count($formDetails);
        $form['cnpcchnl_counter'] = [  
            '#type' => 'hidden',  
            '#attributes' => array("id"=>"cnpcchnl_counter"),
            '#default_value' =>$noofFoms,
        ]; 
        
        
        //echo $noofFoms;
        if($noofFoms==1)
        {
            $showBlock="block";
        }
        else
        {
            $showBlock="none";
        }
        
       $form['cnpc_settings_form_start'] = array(
            '#prefix' => '<div class="cnpc_settings_form" id="cnpc_settings_form"><ol><li>Select your Channel, enter a start date and click SAVE.</li><li>Copy the "shortcode" from pledgeTV Channels page, to your Drupal page. Multiple Channels may be added. Channels will display in order of start date. If start dates overlap, the first Channel in the list will show first.</li></ol>'
            . '<table id="custrows"><tr>
        <th>channel*</th>
       
        <th>Start Date*</th>
        <th>End Date</th>
        </tr><tbody id="dynachnlrows">',
        );
       
        $getInfo = $this->getChannelInfo($reqID);
        //print_r($getInfo);
        
        $campId = $getInfo->cnpstngs_AccountNumber."@".$getInfo->cnpstngs_guid."@".$getInfo->cnpstngs_ID;
        
        $campOpt = $this->getAllChannelsList($campId);
        for($f=0;$f<count($formDetails);$f++)
        {
          
            $fl = $getInfo->cnpstngs_AccountNumber."@".$getInfo->cnpstngs_guid."@".$formDetails[$f]->cnpstngs_ID;
            
            
            //print_r($formList[1]);
            if($formDetails[$f]->cnpchannel_channelEndDate=="0000-00-00 00:00:00")
            {
                $formenddate="";
            }
            else
            {
                $formenddate = date("F d, Y h:i:s a",strtotime($formDetails[$f]->cnpchannel_channelEndDate));
            }
            
            $form['tr_start_'.$f]=array(
                "#prefix"=>"<tr id='row_$f'>",
            );
       
            $form['cnpc_chnl_list_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'select',  
                "#options"=>$campOpt,
                "#default_value"=>$formDetails[$f]->cnpchannel_channelName,
                '#attributes' => array('class' => array('cnpc_chnl_list'),'id'=>'cnpc_chnl_list_'.$f,"data-rowindex"=>$f,"data-opttext"=>$formDetails[$f]->cnpchannel_channelName),
                 "#suffix"=>"</td>"
            );
            
            $form['formguid_'.$f]['#theme_wrappers'] = array();
            $form['chnlstartdate_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'textfield',  
                "#default_value"=>date("F d, Y h:i:s a",strtotime($formDetails[$f]->cnpchannel_channelStartDate)),
                '#attributes' => array('class' => array('chnlstartdate'),'id'=>'chnlstartdate_'.$f,"size"=>25),
                 "#suffix"=>"</td>"
            );
          $form['startdate_'.$f]['#theme_wrappers'] = array();
            $form['chnlenddate_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'textfield',  
                "#default_value"=>$formenddate,
                '#attributes' => array('class' => array('chnlenddate'),'id'=>'chnlenddate_'.$f,"size"=>25),
                 "#suffix"=>"</td>",
            );
            $form['enddate_'.$f]['#theme_wrappers'] = array();
            //print_r($formDetails[$f]->cnpform_id);
             $module_handler = \Drupal::service('module_handler');
            $module_path = $module_handler->getModule('click_pledge_connect')->getPath();
            $form['deletebtn_'.$f]=array(
                "#prefix"=>"<td><a href='#' id='deleteRec_'.$f class='deleteRec' data-target='edit' data-recid='".$formDetails[$f]->cnpchannel_id."' data-index='".$f."'><img src='../../".$module_path."/images/cnpc-trash.png' height='20' width='20'></a>",
                 "#suffix"=>"</td>"
            );
            
            $form['edn']=array( "#suffix"=>"</td>");
        $form['tr_end_'.$f]=array(
            "#suffix"=>"</tr>",
        );
        }
      $form['table_tag_end']=[
                "#suffix"=>"</tbody>
                       </table><a href='#' id='addchannelrows'>Add Channel</a>",
            ];
        $form['cnpc_settings_form_end'] = array(
            '#suffix' => '</div>'
        );
       
         $html1 = '<div><a href="../cnppledgetvchannels" class="cnpc_button_link_bottom">Go back to Channel Groups</a></div>';
        $form['gobackbtn_link_html1'] = array(
            '#type'=> 'markup',
            '#markup'=> $html1,
        );
        
        $form['save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"finalchnlsave"),

        );
       $html2 = '<a href="../cnppledgetvchannels" class="button button--reset js-form-submit form-submit">Close</a>';
        $form['gobackbtn_link_html2'] = array(
            '#type'=> 'markup',
            '#markup'=> $html2,
        );
        
        
        $form['cnpc_hide_buttons_start'] = array(
                '#prefix' => '<div class="cnpc_hide_show_buttons">',
        );
        
       /* $form['reset'] = array(
            '#type' => 'button',
            '#value' => t('Close'),
            '#weight' => 9,
        );*/
         $form['final_save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"updateforms"),
        );
        
       
         
        $form['cnpc_hide_buttons_end'] = array(
                '#suffix' => '</div>'
        );
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
       //parent::submitForm($form, $form_state);
       $accNumber=($form_state->getValue('top'))?$form_state->getValue('top'):"";
       echo $accNumber;exit("sorry");
   }
   public function getRecords()
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpcsettingsdtl';
        $sql = "SELECT * FROM " .$table_name;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
   public function getChannelInfo($id)
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_channelgrp';
	   $sttable_name = $prefix.'dp_cnpcsettingsdtl';
        $sql = "SELECT * FROM " .$table_name." inner join ".$sttable_name." on cnpchannelgrp_cnpstngs_ID = cnpstngs_ID where cnpchannelgrp_ID=".$id;
        $query = $connection->query($sql);
        return $query->fetch();
   }
   public function getChannelDetails($id)
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_channeldtl';
        $sql = "SELECT * FROM " .$table_name." where cnpchannel_cnpchannelgrp_ID=".$id;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
    public function getAllChannelsList($variable)
    {
        $str=explode("@", $variable);
        $connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);

        $module_handler = \Drupal::service('module_handler');
        $host = \Drupal::request()->getSchemeAndHttpHost();
        $module_path = $module_handler->getModule('click_pledge_connect')->getPath()."/Auth2.wsdl";
        $client   = new SoapClient($module_path , $connect);
        $accountguid=$str[1];
        $accountid=$str[0];
        
        $xmlr  = new SimpleXMLElement("<GetPledgeTVChannelList></GetPledgeTVChannelList>");
	$xmlr->addChild('accountId', $accountid);
	$xmlr->addChild('AccountGUID', $accountguid);
	$xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
	$xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
        
        $response = $client->GetPledgeTVChannelList($xmlr); 
        $responsearr =  $response->GetPledgeTVChannelListResult->PledgeTVChannel;
        $camrtrnval[''] = 'Select Channel Name';
        if(count($responsearr) == 1)
        {
            //$camrtrnval[$responsearr->ChannelURLID]=$responsearr->ChannelName;
            $camrtrnval[$responsearr->ChannelURLID]=$responsearr->ChannelName;
        }else{
        for($inc = 0 ; $inc < count($responsearr);$inc++)
        {
            //$camrtrnval[$responsearr[$inc]->ChannelURLID]= $responsearr[$inc]->ChannelName;
            $camrtrnval[$responsearr[$inc]->ChannelURLID]= $responsearr[$inc]->ChannelName;
        }

	 }
         //echo $camrtrnval;
         natcasesort($camrtrnval);
        return  $camrtrnval;
        
    }
    
   
   
   
}

?>
