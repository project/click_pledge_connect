<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;


class AddChannelGroup extends FormBase
{
     
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpcaccount_add_channel_group";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
      
        $form=$this->displayChannelgroupsForm($form, $form_state);
      
        return $form;
   }
   public function displayChannelgroupsForm($form, $form_state)
   {
       
       date_default_timezone_set(drupal_get_user_timezone());
    
        $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a href="cnp_form_help">Click & Pledge</a></li>'
            . '<li><a href="cnp_form">Form Groups</a></li>'
            //. '<li><a href="add_form_group">Add Form Group</a></li>'
           . '<li><a class="cnpccurrent" href="CnPpledgeTVChannels">PledgeTV</a></li>'
		//    . '<li><a class="cnpccurrent" href="add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
        $form['cnpc_heading'] = array(
                '#prefix' => '<div class="cnp_heading1">',
                '#suffix' => '</div>',
        );
        $form['cnpc_channel_group_name'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Channel Group Name*'),  
            '#description' => $this->t('Please enter the channel group name'),  
            '#default_value' =>'',
            '#attributes' => array("id"=>"cnpc_channel_group_name"),
        ];
        $data=$this->getAccountRecords();
       
        $options=array();
        foreach($data as $rec)
        {
            $options[$rec->cnpstngs_AccountNumber."||".$rec->cnpstngs_guid."||".$rec->cnpstngs_ID]=$rec->cnpstngs_frndlyname.' ( '.$rec->cnpstngs_AccountNumber.' )';
        }
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
        $form['cnpc_action'] = [
            '#type' => 'hidden',
            '#default_value' => 'add',
            '#attributes' => array("id"=>"cnpc_action"),
        ];
        
        $form['cnpc_params_popup']=[
            "#prefix"=>'<div id="cnpc_myModal" class="cnpc-modal">
                <div class="cnpc-modal-content">
                  <div class="cnpc-modal-header">
                    <span class="cnpc-close">&times;</span>
                    <h2>URL Parameter(s)</h2>
                  </div>
                  <div class="cnpc-modal-body">'];
       $form['cnpc_params_textarea']=[
            "#prefix"=>'<div class="cnpc-form-group">',
            "#type"=>"textarea",
            "#atrributes"=>array('class' => array('paramsfeild')),
            "#suffix"=>"</div>",
        ];
       $form['cnpc_params_textarea']['#theme_wrappers'] = array();
        $form["cnpc_params_buttons_start"]=[
            "#prefix"=>"</div><div class='cnpc-modal-footer'>",
        ];  
       
        $form['cnpc_params_button_add'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array('class' => array('popupsavebtn'),"id"=>"addparams"),
        );
        $form['cnpc_params_button_close'] = array(
            '#type' => 'button',
            '#value' => 'close',
            '#attributes'=>array('class' => array('popupclosebtn'),"id"=>"cnpc-close"),
        );
         $form["cnpc_params_buttons_end"]=[
            "#suffix"=>"</div></div></div>",
        ];      
            
        
        $form['cnpc_accounts'] = [  
            '#type' => 'select',  
            '#title' => $this->t('Account(s)*'),  
             '#options' => $options,
            '#attributes' => array("id"=>"cnpc_accounts"),
        ]; 
         $form['cnpc_start_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Start Date & Time* [TimeZone: '.drupal_get_user_timezone().']'),  
            '#attributes' => array("id"=>"start_date_time"),
            '#default_value' =>date("F d, Y h:i a"),
         
        ]; 
        $form['cnpc_end_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('End Date & Time'),  
            '#attributes' => array("id"=>"end_date_time"),
            '#default_value' =>'',
        ]; 
         
      
        $form['cnpc_message'] = [  
            '#type' => 'textarea',  
            '#title' => $this->t('No Valid Form Message'),  
            '#attributes' => array("id"=>"cnpc_message"),
            '#default_value' =>'Sorry! This channel is expired',
        ];
        $form['cnpc_status'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Status'),  
            '#options' => array("active"=>"Active","inactive"=>"Inactive"),
           '#attributes' => array("id"=>"cnpc_status"),
        ]; 
        
        $form['cnpcchnl_counter'] = [  
            '#type' => 'hidden',  
            '#attributes' => array("id"=>"cnpcchnl_counter"),
            '#default_value' =>0,
        ]; 
        
        //hidden form Starts here
        
        $form['cnpc_settings_channel_start'] = array(
            '#prefix' => '<div class="cnpc_settings_form" id="cnpc_settings_form"><ol><li>Select your Channel, enter a start date and click SAVE.</li><li>Copy the "shortcode" from pledgeTV Channels page, to your Drupal page. Multiple Channels may be added. Channels will display in order of start date. If start dates overlap, the first Channel in the list will show first.</li></ol>'
            . '<table id="custchnlrows"><tr>
        <th>Channel*</th>
        <th>Start Date/Time*</th>
        <th>End Date/Time</th>
        </tr><tbody id="dynachnlrows"></tbody></table><a href="#" id="addchannelrows">Add Channel</a>',
        );
        $form['cnpc_settings_form_end'] = array(
            '#suffix' => '</div>'
        );

        $form['save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"chnlsavebtn"),
        );
        /*$form['reset'] = array(
            '#type' => 'button',
            '#button_type' => 'reset',
            '#value' => t('Cancel'),
            '#weight' => 9,
            '#validate' => array(),
            '#attributes' => array(
                "id"=>"resetbtn",
                //'onclick' => 'this.form.reset(); return false;',
                'onclick' => 'window.location="cnppledgetvchannels"',
            ),
        );*/
        
       
        $html1 = '<a id="resetbtn" href="cnppledgetvchannels" class="button button--reset js-form-submit form-submit">Close</a>';
        $form['gobackbtn_link_html1'] = array(
            '#type'=> 'markup',
            '#markup'=> $html1,
        );
        
        $form['cnpc_hide_buttons_start'] = array(
                '#prefix' => '<div class="cnpc_hide_show_buttons">',
        );
        
         $form['final_save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"finalchnlsave"),

        );
        $html2 = '<a href="cnppledgetvchannels" class="button button--reset js-form-submit form-submit">Close</a>';
        $form['gobackbtn_link_html2'] = array(
            '#type'=> 'markup',
            '#markup'=> $html2,
        );
       
        
        $form['cnpc_hide_buttons_end'] = array(
                '#suffix' => '</div>'
        );
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
       //parent::submitForm($form, $form_state);
       $accNumber=($form_state->getValue('top'))?$form_state->getValue('top'):"";
       echo $accNumber;exit("sorry");
   }
   public function getAccountRecords()
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpcsettingsdtl';
        $sql = "SELECT * FROM " .$table_name;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
}
?>
