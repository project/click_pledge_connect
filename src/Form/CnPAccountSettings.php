<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;


class CnPAccountSettings extends FormBase
{
    
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpc_account_settings_form";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        //$config=$this->config("cnpcaccount.settings");
        $form=$this->displayAccountSettingsForm($form,  $form_state);
        //return parent::buildForm($form, $form_state);
		return $form;
		
   }
   public function displayAccountSettingsForm($form, $form_state)
   {
       //logo display
       
       //echo $config->get("cnpcaccount.cnpc_account_number");
       //echo $config->get("cnpcaccount.cnpc_account_guid");
       //echo $config->get("cnpcaccount.nickname_cnpc");
        if(count($this->getRecords())>0)
        {
            $form['cnpc_menu'] = array(
                     '#prefix' => '<div class="cnp_heading"><ul>'
                 . '<li><a href="cnp_form_help">Click & Pledge</a></li>'
                 . '<li><a href="cnp_form">Form Groups</a></li>'
                // . '<li><a href="add_form_group">Add Form Group</a></li>'
                 .'<li><a href="cnppledgetvchannels">pledgeTV</a></li>'
                 //.'<li><a href="add_channel_group">Add pledgeTV Channel Group</a></li>'
                 . '<li><a class="cnpccurrent" href="cnp_formssettings">Settings</a></li>'
                 . '</ul>',
                     '#suffix' => '</div>',
             );
        }
       $form['cnpc_error_msg'] = array(
                '#prefix' => '<div class="cnpc_error_msg" id="alertmsg">',
                '#suffix' => '</div>',
        );
       
        /*$form['cnpc_heading'] = array(
                '#prefix' => '<div class="cnp_heading1"><h3>Add New Account</h3>',
                '#suffix' => '</div>',
        );*/
        $form['cnpc_account_number'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Click & Pledge Account Number*'),  
            '#description' => $this->t('Please enter the Account Number of your Connect Account'),  
            '#default_value' => '',
            '#attributes' => array("id"=>"cnpc_account_number"),
        ]; 
        $form['cnpc_account_guid'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Click & Pledge Account GUID*'),  
            '#description' => $this->t('Please enter the Account GUID of your Connect Account'),  
            '#default_value' => '',
            '#attributes' => array("id"=>"cnpc_account_guid"),
        ];
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
         $form['nickname_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => "",
            '#attributes' => array("id"=>"nickname_cnpc"),
        ];
        $form['response_cnpc'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nickname*'),
            '#default_value' => "",
            '#attributes' => array("id"=>"response_cnpc","disabled"=>"disabled"),
            "#prefix"=>"<div class='nickname-div'>",
            "#suffix"=>"</div>",
        ];
        $form['save'] = array(
            '#type' => 'submit',
            '#input' => TRUE,
            '#value' => 'Verify',

        );
        $form['page_break'] = [
            '#prefix' => '<div class="page_break">',
            '#suffix' => '</div>',
        ];
        if(count($this->getRecords())>0)
        {
            $form['table_start'] = array(
                "#prefix"=>"<div class='tablelist'>",
            );
			
            $form['table_tag_start']=[
                    "#prefix"=>"<div><table id='table_id' class='display'>
                            <thead>
                                    <tr>
                                            <th>Nickname</th>
                                            <th>Account Number</th>
                                            <th>GUID</th>
                                            <th>Created Date</th>
                                            <th>Operations</th>
                                    </tr>
                            </thead>
                            <tbody>"];
            $datarec=$this->getRecords();
            //print_r($datarec);
            $k=1;
            foreach($datarec as $rec)
            {
                $form['table_row'.$k]=[
                     "#prefix"=>"<tr>
                        <td>".$rec->cnpstngs_frndlyname."</td>
                        <td>".$rec->cnpstngs_AccountNumber."</td>
                        <td>".$rec->cnpstngs_guid."</td>
                        <td>".date("F d, Y h:i a",strtotime($rec->cnpstngs_Date_Created))."</td>
                        <td><a href='#' data-settid='".$rec->cnpstngs_ID."' data-settaccid='".$rec->cnpstngs_AccountNumber."' data-settguid=".$rec->cnpstngs_guid." class='refreshAccounts' id='refreshacc_".$rec->cnpstngs_ID."' >Refresh</a> | <a href='#' class='settingsDel' id='settingsDel' data-settrecid='".$rec->cnpstngs_ID."'>Delete</a></td>",
                     "#suffix"=>"</tr>",
                 ];
                
                $k++;
            }
             $form['table_tag_end']=[
                 "#suffix"=>"</tbody>
                            </table></div>",
             ];                       
                            
                     
            
            

            //print_r($this->getRecords());
            $form['table_end'] = array(
                "#suffix"=>"</div>",
            );
        }
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
       //parent::submitForm($form, $form_state);
       
        $accNumber=($form_state->getValue('cnpc_account_number'))?$form_state->getValue('cnpc_account_number'):"";
        $accGuid=($form_state->getValue('cnpc_account_guid'))?$form_state->getValue('cnpc_account_guid'):"";
        $nickname=($form_state->getValue('nickname_cnpc'))?$form_state->getValue('nickname_cnpc'):"";
        
		$connection= \Drupal::database();
		$prefix=$connection->tablePrefix();
		$table_name = $prefix.'dp_cnpcsettingsdtl';
		/*$sql = "SELECT * FROM " .$table_name;
		$query = $connection->query($sql);
		$query->allowRowCount = TRUE;*/
		
	\Drupal::database()->insert($table_name)
	->fields([
		'cnpstngs_frndlyname',
		'cnpstngs_AccountNumber',
		'cnpstngs_guid',
	])
	->values(array(
		$nickname,
		$accNumber,
		$accGuid,
	))
	->execute();
		
	        
   }
   public function getRecords()
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpcsettingsdtl';
        $sql = "SELECT * FROM " .$table_name;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
}
