<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Form\FormBase;
class ViewChannels extends FormBase
{
    
 
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpcaccount_viewchannels";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        $form=$this->displayAccountSettingsForm($form, $form_state);
        return $form;
   }
   public function displayAccountSettingsForm($form, $form_state)
   {
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
       $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a href="../cnp_form_help">Click & Pledge</a></li>'
            . '<li><a href="../cnp_form">Form Groups</a></li>'
            //. '<li><a href="../add_form_group">Add Form Group</a></li>'
            .'<li><a class="cnpccurrent" href="../cnppledgetvchannels">pledgeTV</a></li>'
            //.'<li><a href="../add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="../cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
        $path = \Drupal::request()->getpathInfo();
        $arg  = explode('/',$path);
        $reqID=end($arg);
        
        //dp_cnpc_formsdtl
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_channeldtl';
        $sql = "SELECT * FROM " .$table_name." where cnpchannel_cnpchannelgrp_ID=".$reqID;
        $query = $connection->query($sql);
        $allForms = $query->fetchAll();
        
        $form['cnpc_heading'] = array(
            '#prefix' => '<div class="cnp_heading1">',
            '#suffix' => '</div>',
        );
       
        $html = '<a href="../cnppledgetvchannels" class="cnpc_button_link_edit">Go back to Channel Groups</a>';

        $form['gobackbtn_link_html'] = array(
            '#type'=> 'markup',
            '#markup'=> $html,
        );
        
        $form['table_group_start'] = array(
            "#prefix"=>"<div class='tablelist_group'>",
        );
	if(count($allForms)>0)		
        {
           
            $form['table_tag_start']=[
                    "#prefix"=>"<div><table id='table_viewforms' class='table_viewforms'>
                            <thead>
                                    <tr>
                                            <th>ID</th>
                                            <th>Channel Name</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            
                                    </tr>
                            </thead>
                            <tbody>"];
                             $rg=1;
                             foreach ($allForms as $fg)
                             {
                                 if($fg->cnpchannel_channelEndDate=="0000-00-00 00:00:00")
                                 {
                                     $enddate="";
                                 }
                                 else
                                 {
                                     $enddate=date("F d, Y h:i:s a",strtotime($fg->cnpchannel_channelEndDate));
                                 }
                             $form['table_row_form_groups_'.$rg]=[
                                "#prefix"=>"<tr>
                                   <td>".$fg->cnpchannel_id."</td>
                                   <td>".$fg->cnpchannel_channelName."</td>
                                   <td>".date("F d, Y h:i:s a",strtotime($fg->cnpchannel_channelStartDate))."</td>
                                   <td>".$enddate."</td>",
                                "#suffix"=>"</tr>",
                                ];
                             $rg++;
                             }
                               
                           
            $form['table_tag_end']=[
                "#suffix"=>"</tbody>
                       </table></div>",
            ];
            
        }
        else
        {
            $form['no_records'] = array(
            "#prefix"=>"<div><p>No Records Found.</p>",
            "#suffix"=>"</div>",
        );
        }
            
        $form['table_group_end'] = array(
            "#suffix"=>"</div>",
        );
        
        
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
      // parent::submitForm($form, $form_state);

   }
  
}
