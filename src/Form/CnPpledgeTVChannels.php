<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Form\FormBase;

class CnPpledgeTVChannels extends FormBase
{
    
 
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpcaccount_pledgetv_channels";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        $form=$this->displaypledgeTVChannelsForm($form, $form_state);
        return $form;
   }
   public function displaypledgeTVChannelsForm($form, $form_state)
   {
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
       $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a href="cnp_form_help">Click & Pledge</a></li>'
            . '<li><a href="cnp_form">Form Groups</a></li>'
            //. '<li><a href="add_form_group">Add Form Group</a></li>'
            . '<li><a class="cnpccurrent" href="CnPpledgeTVChannels">PledgeTV</a></li>'
		//    . '<li><a href="add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
        $form['cnpc_heading'] = array(
            '#prefix' => '<div class="cnp_heading1">',
            '#suffix' => '</div>',
        );
       
        $html = '<div class="cnpc-btn-center"><a href="add_channel_group" class="cnpc_button_link_add">Add New Channel Group</a></div>';

        $form['button_link_html'] = array(
            '#type'=> 'markup',
            '#markup'=> $html,
        );
        
        $form['table_group_start'] = array(
            "#prefix"=>"<div class='tablelist_group'>",
        );
	if(count($this->getRecords())>0)		
        {
           
            $form['table_tag_start']=[
                    "#prefix"=>"<div><table id='table_form_group' class='table_form_group'>
                            <thead>
                                    <tr>
                                            <th>Group Name</th>
                                            <th>Account#</th>
                                             <th>ShortCode <span class='cnpc_question'>&#63;</span><div class='cnpc_shortcode_tooltip'>Please copy this code and place it in your required content pages, posts or any custom content types. This code will run the series of the channels which has been added to this particular channel Group inside your content page.</div></th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Active Channel(s)</th>
                                            <th>Last Modified</th>
                                            <th>Status</th>
                                            <th>Operations</th>
                                    </tr>
                            </thead>
                            <tbody>"];
                             $channelGroups = $this->getRecords();
                             $rg=1;
                             foreach ($channelGroups as $fg)
                             {
                                 
                                 
                                 if($fg->cnpchannelgrp_channel_EndDate=="0000-00-00 00:00:00")
                                 {
                                    $enddate="";
                                 }
                                 else
                                 {
                                     $enddate = date("F d, Y h:i:s a",strtotime($fg->cnpchannelgrp_channel_EndDate));
                                 }
                             $form['table_row_form_groups_'.$rg]=[
                                "#prefix"=>"<tr>
                                   <td>".$fg->cnpchannelgrp_groupname."</td>
                                   <td>".$fg->cnpstngs_AccountNumber."</td>
                                   <td>".$fg->cnpchannelgrp_shortcode."</td>
                                   <td>".date("F d, Y h:i:s a",strtotime($fg->cnpchannelgrp_channel_StartDate))."</td>
                                   <td>".$enddate."</td>
                                   <td>".count($this->getNoofChannels($fg->cnpchannelgrp_ID))."</td>
                                   <td>".date("F d, Y h:i:s a",strtotime($fg->cnpchannelgrp_Date_Modified))."</td>
                                   <td><a id='formstatus_".$fg->cnpchannelgrp_ID."' class='ptvformstatus' href='' data-fgid='".$fg->cnpchannelgrp_ID."' data-fstatus='".$fg->cnpchannelgrp_status."'>".$fg->cnpchannelgrp_status."</a></td>
                                   <td><a href='viewchannels/".$fg->cnpchannelgrp_ID."'>View</a> | <a href='editchannels/".$fg->cnpchannelgrp_ID."'>Edit</a> | <a id='deletechannelInfo' class='deletechannelInfo' data-recid='".$fg->cnpchannelgrp_ID."' href='#'>Delete</a> </td>",
                                "#suffix"=>"</tr>",
                                ];
                             $rg++;
                             }
                               
                           
            $form['table_tag_end']=[
                "#suffix"=>"</tbody>
                       </table></div>",
            ];
            
        }
        else
        {
            $form['no_records'] = array(
            "#prefix"=>"<div><h3>No Records Found.</h3>",
            "#suffix"=>"</div>",
        );
        }
            
        $form['table_group_end'] = array(
            "#suffix"=>"</div>",
        );
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
      // parent::submitForm($form, $form_state);

   }
   public function getRecords()
   {
      $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_channelgrp';
	    $settingstable_name = $prefix.'dp_cnpcsettingsdtl';
        $sql = "SELECT * FROM " .$table_name ." INNER JOIN ".$settingstable_name." ON cnpchannelgrp_cnpstngs_ID=cnpstngs_ID";
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
   public function getNoofChannels($id)
   {
       $connection= \Drupal::database();
       $prefix=$connection->tablePrefix();
       $table_name = $prefix.'dp_cnpc_channeldtl';
       $sql = "SELECT * FROM " .$table_name." where cnpchannel_cnpchannelgrp_ID=".$id;
       $query = $connection->query($sql);
       return $query->fetchAll();
       //return $query->rowCount();
   }
}
