<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Form\FormBase;

class CnPAccountForm extends FormBase
{
    
 
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpcaccount_settings_form";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        $form=$this->displayAccountSettingsForm($form, $form_state);
        return $form;
   }
   public function displayAccountSettingsForm($form, $form_state)
   {
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
      $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a href="cnp_form_help">Click & Pledge</a></li>'
            . '<li><a class="cnpccurrent" href="cnp_form">Form Groups</a></li>'
            //. '<li><a href="add_form_group">Add Form Group</a></li>'
            .'<li><a href="cnppledgetvchannels">pledgeTV</a></li>'
            //.'<li><a href="add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
        $form['cnpc_heading'] = array(
            '#prefix' => '<div class="cnp_heading1">',
            '#suffix' => '</div>',
        );
       
        $html = '<div class="cnpc-btn-center"><a href="add_form_group" class="cnpc_button_link_add">Add New Form Group</a></div>';

        $form['button_link_html'] = array(
            '#type'=> 'markup',
            '#markup'=> $html,
        );
        
        $form['table_group_start'] = array(
            "#prefix"=>"<div class='tablelist_group'>",
        );
	if(count($this->getRecords())>0)		
        {
           
            $form['table_tag_start']=[
                    "#prefix"=>"<div><table id='table_form_group' class='table_form_group'>
                            <thead>
                                    <tr>
                                            <th>Group Name</th>
                                            <th>Account#</th>
                                            <th>Type</th>
                                            <th>ShortCode<span class='cnpc_question'>&#63;</span><div class='cnpc_shortcode_tooltip'>Please copy this code and place it in your required content pages, posts or any custom content types. This code will run the series of the forms which has been added to this particular Form Group inside your content page.</div></th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Active Form(s)</th>
                                            <th>Last Modified</th>
                                            <th>Status</th>
                                            <th>Operations</th>
                                    </tr>
                            </thead>
                            <tbody>"];
                             $formGroups = $this->getRecords();
                             $rg=1;
                             foreach ($formGroups as $fg)
                             {
                                 
                                 
                                 if($fg->cnpform_Form_EndDate=="0000-00-00 00:00:00")
                                 {
                                    $enddate="";
                                 }
                                 else
                                 {
                                     $enddate = date("F d, Y h:i:s a",strtotime($fg->cnpform_Form_EndDate));
                                 }
                             $form['table_row_form_groups_'.$rg]=[
                                "#prefix"=>"<tr>
                                   <td>".$fg->cnpform_groupname."</td>
                                   <td>".$fg->cnpform_AccountNumber."</td>
                                   <td><span class='cnpform_type'>".$fg->cnpform_type."</span></td>
                                   <td>".$fg->cnpform_shortcode."</td>
                                   <td>".date("F d, Y h:i:s a",strtotime($fg->cnpform_Form_StartDate))."</td>
                                   <td>".$enddate."</td>
                                   <td>".count($this->getNoofForms($fg->cnpform_GID))."</td>
                                   <td>".date("F d, Y h:i:s a",strtotime($fg->cnpform_Date_Modified))."</td>
                                   <td><a id='formstatus_".$fg->cnpform_GID."' class='formstatus' href='' data-fgid='".$fg->cnpform_GID."' data-fstatus='".$fg->cnpform_status."'>".$fg->cnpform_status."</a></td>
                                   <td><a href='viewforms/".$fg->cnpform_GID."'>View</a> | <a href='editforms/".$fg->cnpform_GID."'>Edit</a> | <a id='deleteFormInfo' class='deleteFormInfo' data-recid='".$fg->cnpform_GID."' href='#'>Delete</a> </td>",
                                "#suffix"=>"</tr>",
                                ];
                             $rg++;
                             }
                               
                           
            $form['table_tag_end']=[
                "#suffix"=>"</tbody>
                       </table></div>",
            ];
            
        }
        else
        {
            $form['no_records'] = array(
            "#prefix"=>"<div><h3>No Records Found.</h3>",
            "#suffix"=>"</div>",
        );
        }
            
        $form['table_group_end'] = array(
            "#suffix"=>"</div>",
        );
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
      // parent::submitForm($form, $form_state);

   }
   public function getRecords()
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_forminfo';
        $sql = "SELECT * FROM " .$table_name;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
   public function getNoofForms($id)
   {
       $connection= \Drupal::database();
       $prefix=$connection->tablePrefix();
       $table_name = $prefix.'dp_cnpc_formsdtl';
       $sql = "SELECT * FROM " .$table_name." where cnpform_cnpform_ID=".$id;
       $query = $connection->query($sql);
       return $query->fetchAll();
       //return $query->rowCount();
   }
}
