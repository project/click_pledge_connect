<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;


class PledgeTV extends ConfigFormBase
{
     /**  
    * {@inheritdoc}  
    */ 
   protected function getEditableConfigNames() {
       return [  
          'pledgetv.settings' 
        ]; 
   }
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "pledgetv_form";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        $config=$this->config("pledgetv.settings");
        $form=$this->displayAccountSettingsForm($form, $config, $form_state);
        return parent::buildForm($form, $form_state);
   }
   public function displayAccountSettingsForm($form, $config, $form_state)
   {
       //logo display
       
       //echo $config->get("cnpcaccount.cnpc_account_number");
       //echo $config->get("cnpcaccount.cnpc_account_guid");
       //echo $config->get("cnpcaccount.nickname_cnpc");
       
        $form['cnpc_heading'] = array(
                '#prefix' => '<div class="cnp_heading"><h3>Pledge TV Channels</h3>',
                '#suffix' => '</div>',
        );
        
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
       parent::submitForm($form, $form_state);

   }
}
