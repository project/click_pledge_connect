<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;


class AddFormGroup extends FormBase
{
     
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpcaccount_add_form_group";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        //$config=$this->config("cnpcaccountform.settings");
        $form=$this->displayAccountSettingsForm($form, $form_state);
        //return parent::buildForm($form, $form_state);
        return $form;
   }
   public function displayAccountSettingsForm($form, $form_state)
   {
       //logo display
       date_default_timezone_set(drupal_get_user_timezone());
       //date_default_timezone_set("Asia/Kolkata");
       //echo $config->get("cnpcaccount.cnpc_account_number");
       //echo $config->get("cnpcaccount.cnpc_account_guid");
       //echo $config->get("cnpcaccount.nickname_cnpc");
       $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a href="cnp_form_help">Click & Pledge</a></li>'
            . '<li><a class="cnpccurrent" href="cnp_form">Form Groups</a></li>'
           // . '<li><a class="cnpccurrent" href="add_form_group">Add Form Group</a></li>'
            .'<li><a href="cnppledgetvchannels">pledgeTV</a></li>'
            //.'<li><a href="add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
        $form['cnpc_heading'] = array(
                '#prefix' => '<div class="cnp_heading1">',
                '#suffix' => '</div>',
        );
        $form['cnpc_form_group_name'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Form Group Name*'),  
            '#description' => $this->t('Please enter the form group name'),  
            '#default_value' =>'',
            '#attributes' => array("id"=>"cnpc_form_group_name"),
        ];
        $data=$this->getRecords();
        if(count($data)>0)
        {
            if(count($data)==1)
            {
                $disabled=TRUE;
            }
            else
            {
                $disabled=FALSE;
            }
        }
        else
        {
            $disabled=false;
        }
        
        $options=array();
        foreach($data as $rec)
        {
            $options[$rec->cnpstngs_AccountNumber."||".$rec->cnpstngs_guid."||".$rec->cnpstngs_ID]=$rec->cnpstngs_frndlyname.' ( '.$rec->cnpstngs_AccountNumber.' ) ';
        }
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
        $form['cnpc_action'] = [
            '#type' => 'hidden',
            '#default_value' => 'add',
            '#attributes' => array("id"=>"cnpc_action"),
        ];
        /*URL Params popup Starts*/
        $form['cnpc_params_popup']=[
            "#prefix"=>'<div id="cnpc_myModal" class="cnpc-modal">
                <div class="cnpc-modal-content">
                  <div class="cnpc-modal-header">
                    <span class="cnpc-close">&times;</span>
                    <h2>URL Parameter(s)</h2>
                  </div>
                  <div class="cnpc-modal-body">'];
       $form['cnpc_params_textarea']=[
            "#prefix"=>'<div class="cnpc-form-group-params">',
            "#type"=>"textarea",
            "#atrributes"=>array('class' => array('paramsfeild')),
            "#suffix"=>"</div>",
        ];
       $form['cnpc_params_textarea']['#theme_wrappers'] = array();
        $form["cnpc_params_buttons_start"]=[
            "#prefix"=>"</div><div class='cnpc-modal-footer'>",
        ];  
       
        $form['cnpc_params_button_add'] = array(
            "#prefix"=>'<div>',
            '#type' => 'button',
            '#value' => 'Add',
            '#attributes'=>array('class' => array('popupsavebtn'),"id"=>"addparams"),
            "#suffix"=>'</div>',
        );
        $form['cnpc_params_button_close'] = array(
            "#prefix"=>'<div>',
            '#type' => 'button',
            '#value' => 'Close',
            '#attributes'=>array('class' => array('popupclosebtn'),"id"=>"closeparams"),
            "#suffix"=>'</div>',
        );
        $form['cnpc_params_button_saveclose'] = array(
            "#prefix"=>'<div>',
            '#type' => 'button',
            '#value' => 'Save & Close',
            '#attributes'=>array('class' => array('popupsaveclosebtn'),"id"=>"savecloseparams"),
            "#suffix"=>'</div>',
        );
        $html22 = '</div><p class="cnpc_note_text"><b>Note:</b> Separate with ampersand (&) <a target="_blank" href="https://support.clickandpledge.com/s/article/connect-url-parameters1"><b>Read More...</b></a></p>';
        $form['readmore_link_html22'] = array(
            '#type'=> 'markup',
            '#markup'=> $html22,
        );
        $form["cnpc_params_buttons_end"]=[
            "#suffix"=>"</div></div>",
        ];      
        
        /*URL Params popup Ended*/

        $form['cnpc_accounts'] = [  
            '#type' => 'select',  
            '#title' => $this->t('Account(s)*'),  
             '#options' => $options,
            '#attributes' => array("id"=>"cnpc_accounts"),
            '#disabled' => $disabled,
        ]; 
         $form['cnpc_start_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Start Date & Time* [TimeZone: '.drupal_get_user_timezone().']'),  
            '#attributes' => array("id"=>"start_date_time"),
            '#default_value' =>date("F d, Y h:i a"),
            //'#default_value' => drupal_get_user_timezone(),
            //'#default_value' => date_default_timezone_get(),
        ]; 
        $form['cnpc_end_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('End Date & Time'),  
            '#attributes' => array("id"=>"end_date_time"),
            '#default_value' =>'',
        ]; 
         
        $form['cnpc_display_type'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Display Type'),  
            '#options' => array("inline"=>"Inline","popup"=>"Overlay"),
           '#attributes' => array("id"=>"cnpc_display_type"),
        ];
        //hidden form overlay
        $form['overlay_wrapper_start'] = array(
            '#prefix' => '<div class="overlay_wrapper">',   
        );
        
        $form['cnpc_link_type'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Link Type*'),  
            '#options' => array("button"=>"Button","image"=>"Image","text"=>"Text"),
           '#attributes' => array("id"=>"cnpc_link_type"),
        ];
        $form['cnpc_buton_label'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Button Label*'),  
            '#attributes' => array("id"=>"cnpc_btn_lbl"),
            '#default_value' =>'',
        ];
        $form['cnpc_upload_image'] = [  
            '#type' => 'file',  
            '#title' => $this->t('Upload Image*'),  
            '#attributes' => array("id"=>"cnpc_upload_image"),
            '#default_value' =>'',
        ]; 
        $form['overlay_wrapper_end'] = array(
            '#suffix' => '</div>',
        );
        
        $form['cnpc_message'] = [  
            '#type' => 'textarea',  
            '#title' => $this->t('No Valid Form Message'),  
            '#attributes' => array("id"=>"cnpc_message"),
            '#default_value' =>'No donations are accepted at this time',
        ];
        $form['cnpc_status'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Status'),  
            '#options' => array("active"=>"Active","inactive"=>"Inactive"),
           '#attributes' => array("id"=>"cnpc_status"),
        ]; 
        
        $form['cnpc_counter'] = [  
            '#type' => 'hidden',  
            '#attributes' => array("id"=>"cnpc_counter"),
            '#default_value' =>0,
        ]; 
        
        //hidden form Starts here
        
        $form['cnpc_settings_form_start'] = array(
            '#prefix' => '<div class="cnpc_settings_form" id="cnpc_settings_form">Select your Connect campaign, choose a payment form, enter a start date and click SAVE. Copy the shortcode from Click & Pledge Form Groups page to enter on your WordPress page. Multiple forms may be added to display in order by start date. If dates overlap, the first form in the list will show.'
            . '<table id="custrows"><tr>
        <th>Campaign*</th>
        <th>Form*</th>
        <th>Form GUID*</th>
        <th>Start Date/Time*</th>
        <th>End Date/Time</th>
        </tr><tbody id="dynarows"></tbody></table><a href="#" id="addrows">Add Form</a>',
        );
        $form['cnpc_settings_form_end'] = array(
            '#suffix' => '</div>'
        );

        $form['save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"savebtn"),

        );
        /*$form['reset'] = array(
            '#type' => 'button',
            '#button_type' => 'reset',
            '#value' => t('Cancel'),
            '#weight' => 9,
            '#validate' => array(),
            '#attributes' => array(
                "id"=>"resetbtn",
                'onclick' => 'this.form.reset(); return false;',
            ),
        );*/
        $html2 = '<a id="resetbtn" href="cnp_form" class="button button--reset js-form-submit form-submit">Close</a>';
        $form['gobackbtn_link_html2'] = array(
            '#type'=> 'markup',
            '#markup'=> $html2,
        );
        
        $form['cnpc_hide_buttons_start'] = array(
                '#prefix' => '<div class="cnpc_hide_show_buttons">',
        );
        
         $form['final_save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"finalsave"),
        );
          $html3 = '<a href="cnp_form" class="button button--reset js-form-submit form-submit">Close</a>';
        $form['gobackbtn_link_html3'] = array(
            '#type'=> 'markup',
            '#markup'=> $html3,
        );
        /*$form['reset'] = array(
            '#type' => 'button',
            '#button_type' => 'reset',
            '#value' => t('Cancel'),
            '#weight' => 9,
            '#validate' => array(),
            '#attributes' => array(
                  'onclick' => 'this.form.reset(); return false;',
            ),
        );*/
        
        $form['cnpc_hide_buttons_end'] = array(
                '#suffix' => '</div>'
        );
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
       //parent::submitForm($form, $form_state);
       $accNumber=($form_state->getValue('top'))?$form_state->getValue('top'):"";
       echo $accNumber;exit("sorry");
   }
   public function getRecords()
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpcsettingsdtl';
        $sql = "SELECT * FROM " .$table_name;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
}
?>
