<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface; 
use \SoapClient;
use SimpleXMLElement;

class EditForms extends FormBase
{
    const CFCNP_PLUGIN_UID = "14059359-D8E8-41C3-B628-E7E030537905";
    const CFCNP_PLUGIN_SKY = "5DC1B75A-7EFA-4C01-BDCD-E02C536313A3";
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpc_edit_form_group";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
        //$config=$this->config("cnpcaccountform.settings");
        $form=$this->displayEditCnpcForm($form, $form_state);
        //return parent::buildForm($form, $form_state);
        return $form;
   }
   public function displayEditCnpcForm($form, $form_state)
   {
       //logo display
       date_default_timezone_set(drupal_get_user_timezone());
       
       /*$module_handler = \Drupal::service('module_handler');
       $host = \Drupal::request()->getSchemeAndHttpHost();
       $module_path = $module_handler->getModule('click_pledge_connect')->getPath()."/banners/smile.jpg";
       $imagePath=$host.base_path().$module_path;*/
       
        $path = \Drupal::request()->getpathInfo();
        $arg  = explode('/',$path);
        $reqID=end($arg);
        $formInfo = $this->getFormInfo($reqID);
        $formDetails = $this->getFormDetails($reqID);
        //print_r($formInfo);
       $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a href="../cnp_form_help">Click & Pledge</a></li>'
            . '<li><a class="cnpccurrent" href="../cnp_form">Form Groups</a></li>'
            //. '<li><a href="../add_form_group">Add Form Group</a></li>'
            .'<li><a href="../cnppledgetvchannels">pledgeTV</a></li>'
            //.'<li><a href="../add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="../cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
        $html = '<a href="../cnp_form" class="cnpc_button_link_edit">Go back to Form Groups</a>';

        $form['gobackbtn_link_html'] = array(
            '#type'=> 'markup',
            '#markup'=> $html,
        );
        $form['cnpc_heading'] = array(
                '#prefix' => '<div class="cnp_heading1">',
                '#suffix' => '</div>',
        );
        $form['cnpc_form_group_name'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Form Group Name*'),  
            '#description' => $this->t('Please enter the form group name'),  
            '#default_value' =>$formInfo->cnpform_groupname,
            '#attributes' => array("id"=>"cnpc_form_group_name"),
        ];
        $data=$this->getRecords();
        if(count($data)>0)
        {
            if(count($data)==1)
            {
                $disabled=TRUE;
            }
            else
            {
                $disabled=FALSE;
            }
        }
        else
        {
            $disabled=false;
        }
        $options=array();
        foreach($data as $rec)
        {
            $options[$rec->cnpstngs_AccountNumber."||".$rec->cnpstngs_guid."||".$rec->cnpstngs_ID]=$rec->cnpstngs_frndlyname.' ( '.$rec->cnpstngs_AccountNumber.' )';
        }
 
        $form['base_url_cnpc'] = [
            '#type' => 'hidden',
            '#default_value' => base_path(),
            '#attributes' => array("id"=>"base_url_cnpc"),
        ];
        $form['cnpc_edit_id'] = [
            '#type' => 'hidden',
            '#default_value' => $reqID,
            '#attributes' => array("id"=>"cnpc_edit_id"),
        ];
        //popup textarea for url params
        $form['cnpc_params_popup']=[
          "#prefix"=>'<div id="cnpc_myModal" class="cnpc-modal">
              <div class="cnpc-modal-content">
                <div class="cnpc-modal-header">
                  <span class="cnpc-close">&times;</span>
                  <h2>URL Parameter(s)</h2>
                </div>
                <div class="cnpc-modal-body">'];
       $form['cnpc_params_textarea']=[
            "#prefix"=>'<div class="cnpc-form-group-params">',
            "#type"=>"textarea",
            "#atrributes"=>array('class' => array('paramsfeild')),
            "#suffix"=>"</div>",
        ];
       $form['cnpc_params_textarea']['#theme_wrappers'] = array();
        $form["cnpc_params_buttons_start"]=[
            "#prefix"=>"</div><div class='cnpc-modal-footer'>",
        ];  
       
         $form['cnpc_params_button_add'] = array(
            "#prefix"=>'<div>',
            '#type' => 'button',
            '#value' => 'Add',
            '#attributes'=>array('class' => array('popupsavebtn'),"id"=>"addparams"),
            "#suffix"=>'</div>',
        );
        $form['cnpc_params_button_close'] = array(
            "#prefix"=>'<div>',
            '#type' => 'button',
            '#value' => 'Close',
            '#attributes'=>array('class' => array('popupclosebtn'),"id"=>"closeparams"),
            "#suffix"=>'</div>',
        );
        $form['cnpc_params_button_saveclose'] = array(
            "#prefix"=>'<div>',
            '#type' => 'button',
            '#value' => 'Save & Close',
            '#attributes'=>array('class' => array('popupsaveclosebtn'),"id"=>"savecloseparams"),
            "#suffix"=>'</div>',
        );
        $html22 = '</div><p class="cnpc_note_text"><b>Note:</b> Separate with ampersand (&) <a target="_blank" href="https://support.clickandpledge.com/s/article/connect-url-parameters1"><b>Read More...</b></a></p>';
        $form['readmore_link_html22'] = array(
            '#type'=> 'markup',
            '#markup'=> $html22,
        );
        $form["cnpc_params_buttons_end"]=[
            "#suffix"=>"</div></div>",
        ];      
        
        
        $form['cnpc_action'] = [
            '#type' => 'hidden',
            '#default_value' => 'edit',
            '#attributes' => array("id"=>"cnpc_action"),
        ];
        $form['cnpc_accounts'] = [  
            '#type' => 'select',  
            '#title' => $this->t('Account(s)*'),  
             '#options' => $options,
             '#default_value' => "",
            '#attributes' => array("id"=>"cnpc_accounts"),
            '#disabled' => $disabled  ,
             //"#default_value" => ,
        ]; 
         $form['cnpc_start_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Start Date & Time* [TimeZone: '.drupal_get_user_timezone().' ]'),  
            '#attributes' => array("id"=>"start_date_time"),
            '#default_value' =>date("F d, Y h:i:s a",strtotime($formInfo->cnpform_Form_StartDate)),
            //'#default_value' => drupal_get_user_timezone(),
            //'#default_value' => date_default_timezone_get(),
        ]; 
         if($formInfo->cnpform_Form_EndDate=="0000-00-00 00:00:00")
         {
             $ed="";
         }
         else
         {
             
             $ed = date("F d, Y h:i:s a",strtotime($formInfo->cnpform_Form_EndDate));
         }
        $form['cnpc_end_date_time'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('End Date & Time'),  
            '#attributes' => array("id"=>"end_date_time"),
            '#default_value' =>$ed,
        ]; 
         
        $form['cnpc_display_type'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Display Type'),  
            '#options' => array("inline"=>"Inline","popup"=>"Overlay"),
           '#attributes' => array("id"=>"cnpc_display_type"),
             '#default_value'=>$formInfo->cnpform_type,
        ];
        //hidden form overlay
        $form['overlay_wrapper_start'] = array(
            '#prefix' => '<div class="overlay_wrapper">',   
        );
        
        $form['cnpc_link_type'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Link Type*'),  
            '#options' => array("button"=>"Button","image"=>"Image","text"=>"Text"),
           '#attributes' => array("id"=>"cnpc_link_type"),
            '#default_value'=>$formInfo->cnpform_ptype,
        ];
        $form['cnpc_buton_label'] = [  
            '#type' => 'textfield',  
            '#title' => $this->t('Button Label*'),  
            '#attributes' => array("id"=>"cnpc_btn_lbl"),
            '#default_value' =>$formInfo->cnpform_text,
        ];
        $form['cnpc_upload_image'] = [  
            '#type' => 'file',  
            '#title' => $this->t('Upload Image*'),  
            '#attributes' => array("id"=>"cnpc_upload_image"),
            '#default_value' =>"",
        ]; //
        $form['cnpc_upload_image_hidden'] = [  
            '#type' => 'hidden',  
            '#attributes' => array("id"=>"cnpc_upload_image_hidden"),
            '#default_value' =>$formInfo->cnpform_img,
        ];
        $form['overlay_wrapper_end'] = array(
            '#suffix' => '</div>',
        );
        
        $form['cnpc_message'] = [  
            '#type' => 'textarea',  
            '#title' => $this->t('No Valid Form Message'),  
            '#attributes' => array("id"=>"cnpc_message"),
            '#default_value' =>$formInfo->cnpform_custommsg,
        ];
        $form['cnpc_status'] = [  
           '#type' => 'select',  
           '#title' => $this->t('Status'),  
            '#options' => array("active"=>"Active","inactive"=>"Inactive"),
           '#attributes' => array("id"=>"cnpc_status"),
            '#default_value' =>$formInfo->cnpform_status,
        ]; 
        
        //hidden form Starts here
       
         $noofFoms=count($formDetails);
        $form['cnpc_counter'] = [  
            '#type' => 'hidden',  
            '#attributes' => array("id"=>"cnpc_counter"),
            '#default_value' =>$noofFoms,
        ]; 
        
        
        //echo $noofFoms;
        if($noofFoms==1)
        {
            $showBlock="block";
        }
        else
        {
            $showBlock="none";
        }
        
        $form['cnpc_settings_form_start'] = array(
            '#prefix' => '<div class="cnpc_settings_form" id="cnpc_settings_form">Select your Connect campaign, choose a payment form, enter a start date and click SAVE. Copy the shortcode from Click & Pledge Form Groups page to enter on your WordPress page. Multiple forms may be added to display in order by start date. If dates overlap, the first form in the list will show.'
            . '<table id="custrows"><tr>
        <th>Campaign*</th>
        <th>Form*</th>
        <th>Form GUID*</th>
        <th>Start Date/Time</th>
        <th>End Date/Time</th>
        </tr><tbody id="dynarows">',
        );
       
        $getInfo = $this->getFormInfo($reqID);
        //print_r($getInfo);
        
        $campId = $getInfo->cnpform_AccountNumber."@".$getInfo->cnpform_guid."@".$getInfo->cnpform_cnpstngs_ID;
        
        $campOpt = $this->getAllCampaignsList($campId);
        for($f=0;$f<count($formDetails);$f++)
        {
          
            $fl = $getInfo->cnpform_AccountNumber."@".$getInfo->cnpform_guid."@".$formDetails[$f]->cnpform_CampaignName;
            $formList = $this->getActivityFormsList($fl);
            
            //print_r($formList[1]);
            if($formDetails[$f]->cnpform_FormEndDate=="0000-00-00 00:00:00")
            {
                $formenddate="";
            }
            else
            {
                $formenddate = date("F d, Y h:i:s a",strtotime($formDetails[$f]->cnpform_FormEndDate));
            }
            
            $form['tr_start_'.$f]=array(
                "#prefix"=>"<tr id='row_$f'>",
            );
       
            $form['cnpc_camp_list_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'select',  
                "#options"=>$campOpt,
                "#default_value"=>$formDetails[$f]->cnpform_CampaignName,
                '#attributes' => array('class' => array('cnpc_camp_list'),'id'=>'cnpc_camp_list_'.$f,"data-rowindex"=>$f),
                 "#suffix"=>"</td>"
            );
             $form['cnpc_camp_list_'.$f]['#theme_wrappers'] = array();
            $form['cnpc_form_list_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'select',  
                "#options"=>$formList,
                "#default_value"=>$formDetails[$f]->cnpform_FormName,
                '#attributes' => array('class' => array('cnpc_form_list'),'id'=>'cnpc_form_list_'.$f,"data-formindex"=>$f,"data-opttext"=>$formDetails[$f]->cnpform_FormName),
                 "#suffix"=>"</td>" 
            );
            
             $form['cnpc_form_list_'.$f]['#theme_wrappers'] = array();
            $form['formguid_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'textfield',  
                "#default_value"=>$formDetails[$f]->cnpform_GUID,
                '#attributes' => array('id'=>'formguid_'.$f,"disabled"=>"disabled","size"=>25),
                 "#suffix"=>"</td>"
            );
            $form['formguid_'.$f]['#theme_wrappers'] = array();
            $form['startdate_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'textfield',  
                "#default_value"=>date("F d, Y h:i:s a",strtotime($formDetails[$f]->cnpform_FormStartDate)),
                '#attributes' => array('class' => array('cnpcstartdate'),'id'=>'startdate_'.$f,"size"=>25),
                 "#suffix"=>"</td>"
            );
          $form['startdate_'.$f]['#theme_wrappers'] = array();
            $form['enddate_'.$f]=array(
                "#prefix"=>"<td>",
                '#type' => 'textfield',  
                "#default_value"=>$formenddate,
                '#attributes' => array('class' => array('cnpcenddate'),'id'=>'enddate_'.$f,"size"=>25),
                 "#suffix"=>"</td>",
            );
            $form['enddate_'.$f]['#theme_wrappers'] = array();
            $form['hidden_params_'.$f] = [
                '#type' => 'hidden',
                '#default_value' => $formDetails[$f]->cnpform_urlparameters,
                '#attributes' => array("id"=>"hidden_params_".$f),
            ];
            //print_r($formDetails[$f]->cnpform_id);
            $module_handler = \Drupal::service('module_handler');
            $module_path = $module_handler->getModule('click_pledge_connect')->getPath();
            $form['deletebtn_'.$f]=array(
                "#prefix"=>"<td><a href='#' class='cnpc-myBtn' data-index='".$f."'>URL<b>+</b></a> &nbsp; <a href='#' id='deleteRec_'.$f class='deleteRec' data-target='edit' data-recid='".$formDetails[$f]->cnpform_id."' data-index='".$f."'><img src='../../".$module_path."/images/cnpc-trash.png' height='20' width='20'></a>",
                 "#suffix"=>"</td>"
            );
            
            $form['edn']=array( "#suffix"=>"</td>");
        $form['tr_end_'.$f]=array(
            "#suffix"=>"</tr>",
        );
        }
         $form['table_tag_end']=[
                "#suffix"=>"</tbody>
                       </table><a href='#' id='addrows'>Add Form</a>",
            ];
        $html1 = '<a href="../cnp_form" class="cnpc_button_link_bottom">Go back to Form Groups</a>';

        $form['gobackbtn_link_html1'] = array(
            '#type'=> 'markup',
            '#markup'=> $html1,
        );
        $form['cnpc_settings_form_end'] = array(
            '#suffix' => '</div>'
        );
      
        $form['save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"finalsave"),

        );
         $html2 = '<a id="resetbtn" href="../cnp_form" class="button button--reset js-form-submit form-submit">Close</a>';
        $form['gobackbtn_link_html2'] = array(
            '#type'=> 'markup',
            '#markup'=> $html2,
        );
        
        $form['cnpc_hide_buttons_start'] = array(
                '#prefix' => '<div class="cnpc_hide_show_buttons">',
        );
        
         $form['final_save'] = array(
            '#type' => 'button',
            '#value' => 'Save',
            '#attributes'=>array("id"=>"updateforms"),

        );
        /*$form['reset'] = array(
            '#type' => 'button',
            '#button_type' => 'reset',
            '#value' => t('Cancel'),
            '#weight' => 9,
            '#validate' => array(),
            '#attributes' => array(
                  'onclick' => 'this.form.reset(); return false;',
            ),
        );*/
        
        $form['cnpc_hide_buttons_end'] = array(
                '#suffix' => '</div>'
        );
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
       //parent::submitForm($form, $form_state);
       $accNumber=($form_state->getValue('top'))?$form_state->getValue('top'):"";
       echo $accNumber;exit("sorry");
   }
   public function getRecords()
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpcsettingsdtl';
        $sql = "SELECT * FROM " .$table_name;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
   public function getFormInfo($id)
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_forminfo';
        $sql = "SELECT * FROM " .$table_name." where cnpform_GID=".$id;
        $query = $connection->query($sql);
        return $query->fetch();
   }
   public function getFormDetails($id)
   {
        $connection= \Drupal::database();
        $prefix=$connection->tablePrefix();
        $table_name = $prefix.'dp_cnpc_formsdtl';
        $sql = "SELECT * FROM " .$table_name." where cnpform_cnpform_ID=".$id;
        $query = $connection->query($sql);
        return $query->fetchAll();
   }
    public function getAllCampaignsList($variable)
    {
        $str=explode("@", $variable);
	$connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
	$client   = new SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
        $accountguid=$str[1];
        $accountid=$str[0];
        
        $xmlr  = new SimpleXMLElement("<GetActiveCampaignList2></GetActiveCampaignList2>");
	$xmlr->addChild('accountId', $accountid);
	$xmlr->addChild('AccountGUID', $accountguid);
	$xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
	$xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
        
        $response = $client->GetActiveCampaignList2($xmlr); 
        $responsearr =  $response->GetActiveCampaignList2Result->connectCampaign;
        $camrtrnval[''] = 'Select Campaign Name';
        if(count($responsearr) == 1)
        {
            $camrtrnval[$responsearr->alias]=$responsearr->name;
        }else{
        for($inc = 0 ; $inc < count($responsearr);$inc++)
        {
            $camrtrnval[$responsearr[$inc]->alias]= $responsearr[$inc]->name;
        }

	}
         //echo $camrtrnval;
         natcasesort($camrtrnval);
        return  $camrtrnval;
        
    }
    
    public function getActivityFormsList($variable)
    {
        $str=explode("@", $variable);
        $connect  = array('soap_version' => SOAP_1_1, 'trace' => 1, 'exceptions' => 0);
	$client   = new SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
        $cnpaccountID      = $str[0];
        $cnpaccountguidID  =$str[1];
        $cnpcampaignId     = $str[2];
        $xmlr  = new SimpleXMLElement("<GetActiveFormList2></GetActiveFormList2>");
        $xmlr->addChild('accountId', $cnpaccountID);
        $xmlr->addChild('AccountGUID', $cnpaccountguidID);
        $xmlr->addChild('username', self::CFCNP_PLUGIN_UID);
        $xmlr->addChild('password', self::CFCNP_PLUGIN_SKY);
        $xmlr->addChild('campaignAlias', $cnpcampaignId);

        $frmresponse    =  $client->GetActiveFormList2($xmlr); 	
        $frmresponsearr =  $frmresponse->GetActiveFormList2Result->form;	
        $rtrnval[''] = "Select Form Name";
        if(count($frmresponsearr) == 1)
        {
            $rtrnval1[$frmresponsearr->formGUID]=$frmresponsearr->formName;
            $rtrnval[$frmresponsearr->formGUID]=$frmresponsearr->formName;
        }else{
        for($finc = 0 ; $finc < count($frmresponsearr);$finc++)
        {
            $rtrnval1[$frmresponsearr[$finc]->formGUID]=$frmresponsearr[$finc]->formName;
            $rtrnval[$frmresponsearr[$finc]->formGUID]=$frmresponsearr[$finc]->formName;
        }

	}
        //return array($rtrnval,$rtrnval1);
        natcasesort($rtrnval);
        return $rtrnval;
    }
   
   
}

?>
