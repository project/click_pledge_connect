<?php
namespace Drupal\click_pledge_connect\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;  
use Symfony\Component\HttpFoundation\RedirectResponse;


class CnPFormsHelp extends FormBase
{
   
   /*
    * {@inheritdoc}
    */
   public function getFormId() {
       return "cnpformshelp_form";
   }
   public function buildForm(array $form, FormStateInterface $form_state) {
       
        $form=$this->displayAccountSettingsForm($form, $config, $form_state);
        return $form;
   }
   public function displayAccountSettingsForm($form, $config, $form_state)
   {
       //logo display
       
       //echo $config->get("cnpcaccount.cnpc_account_number");
       //echo $config->get("cnpcaccount.cnpc_account_guid");
       //echo $config->get("cnpcaccount.nickname_cnpc");
       
        $form['cnpc_heading'] = array(
                '#prefix' => '<div class="cnp_heading">',
                '#suffix' => '</div>',
        );
        $form['cnpc_menu'] = array(
                '#prefix' => '<div class="cnp_heading"><ul>'
            . '<li><a class="cnpccurrent" href="cnp_form_help">Click & Pledge</a></li>'
            . '<li><a href="cnp_form">Form Groups</a></li>'
            //. '<li><a href="add_form_group">Add Form Group</a></li>'
            .'<li><a href="cnppledgetvchannels">pledgeTV</a></li>'
            //.'<li><a href="add_channel_group">Add pledgeTV Channel Group</a></li>'
            . '<li><a href="cnp_formssettings">Settings</a></li>'
            . '</ul>',
                '#suffix' => '</div>',
        );
        $html = '<div class="cnpc_information">
        
            <div class="instructions">
                
                <p>For quick start, follow these instructions</p>
                <ol>
                        <li>Go to <a href="cnp_formssettings">Settings</a> page and add Click &amp; Pledge Account Number, Account GUID and Nickname. Find Account GUID in your Click &amp; Pledge Connect. Find out <a href="http://help.clickandpledge.com/customer/portal/articles/2228929-how-to-locate-account-id-api-account-guid" target="_blank">how</a>.</li>
                        <li>Once the Account information is added, additional menu items will appear and you may add <strong>"<a href="">Form Groups</a>"</strong>.</li>
                        <li>After saving the Form Group details, a new table will appear on the same page where you may select one or more forms from different campaigns. <br><small><strong>Note:</strong> All campaigns and forms are retrieved from <a href="https://connect.clickandpledge.com/" target="_blank">Click &amp; Pledge Connect</a>.</small></li>
                </ol>
                <p>
                        For step by step guide follow our manual <a href="">here</a>.
                </p>
            </div>
      </div>';

    $form['display_html'] = array(
        '#type'=> 'markup',
        '#markup'=> $html,
    );
        return $form;
   }
   public function validateForm(array &$form, FormStateInterface $form_state) {
       //parent::validateForm($form, $form_state);
   }
   public function submitForm(array &$form, FormStateInterface $form_state) {
       parent::submitForm($form, $form_state);

   }
}
