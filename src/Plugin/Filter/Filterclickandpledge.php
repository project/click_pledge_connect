<?php
/**
 * @file
 * Contains Drupal\click_pledge_connect\Plugin\Filter\Filterclickandpledge
 */
namespace Drupal\click_pledge_connect\Plugin\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Provides a filter to help celebrate good times!
 *
 * @Filter(
 *   id = "filter_clickandpledge",
 *   title = @Translation("Click & Pledge Shortcode"),
 *   description = @Translation("Easy to add process for adding Connect forms and pledgeTV"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class Filterclickandpledge extends FilterBase {
  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
	
    $cnpformshortcode     =  $this->settings['cnpshortcode_cnpform'] ? 'yes' : '';
	$cnppledgetvshortcode =  $this->settings['cnpshortcode_cnppledgeTV'] ? 'yes' : '';
	$output="";
	$cnpsplitcntnt = preg_split('!(\[{1,2}.*?\]{1,2})!', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
	//pledgeTV Code Start
	
	if($cnppledgetvshortcode != ''){
		
	 $shortcode_tokens = &drupal_static(__FUNCTION__);
	  $cnpresult ="";$mpos="";$cnpchnlgrpname = "";
		foreach ($cnpsplitcntnt as $cntnt) {
		  if (!$cntnt) {
			continue;
		  }
   		  if(($cntnt[0] == '[') && (substr($cntnt, -1, 1) == ']')) {
			  $mpos =  array_search("[cnppledgetv]",$cnpsplitcntnt);
			  		  if($mpos != ""){
						$srchshortcode   = $cnpsplitcntnt[$mpos].$cnpsplitcntnt[$mpos+1].$cnpsplitcntnt[$mpos+2];}
			            $chkshortcodexit = $this->CNPDPconnect_isExistchannelShortcode($srchshortcode);

	if($chkshortcodexit)
	{
		 $cnporiginaltext = $srchshortcode; 
		 $mpostn          =  $mpos;
		 $cnpgrpname      = $cnpsplitcntnt[$mpostn+1];
		 $cnpchannelid    = $this->CNPDPconnect_getchannelsofGroup($cnpgrpname);
		 $cntnt           = substr($cntnt, 1, -1);
         $cntnt           = trim($cntnt);
         $cnpts           = explode(' ', $cntnt);
         $cnptag          = array_shift($cnpts);
        // $cnptag          = trim($cnptag, '/');
		if ($cnptag == "cnppledgetv") {
	
		if(count($cnpchannelid) >=1){
		$rtrnstrarr ="";
		for($frminc=0;$frminc <	count($cnpchannelid); $frminc++)
		{
			
 		$attrs = array('class' => 'cnp_pledgetv_wrapper', 'data-channel' => $cnpchannelid[$frminc],'data-iframe-width'=>'100%','data-iframe-height'=>'315') ;
		
		$attrs_string = '';
		if(!empty( $attrs ) ) {

			foreach ( $attrs as $key => $value ) {
				$attrs_string .= "$key='" .  $value . "' ";
			}
			$attrs = ltrim( $attrs_string );

	  	 }
		$rtrnstrarr .="<script>var list = document.getElementsByTagName('script');
				var i = list.length, flag = false;
				while (i--) { 
					if (list[i].src === 'https://pledge.tv/library/js/pledgetv.js') {
						flag = true;
						break;
					}
				}

				if (!flag) {
					var tag = document.createElement('script');
					tag.src = 'https://pledge.tv/library/js/pledgetv.js';
					document.getElementsByTagName('body')[0].appendChild(tag);
				}</script>";
			
			}
	 $rtrnstrarr .='<div '.$attrs .'></div>';
	
			}else{
			 $rtrnstrarr = $this->CNPDPconnect_getGroupchnlCustomerrmsg($srchshortcode);
	 

		}
			 $output .= $rtrnstrarr;
	     }
	}
	else
	{
	  $output =$this->CNPDPconnect_getGroupchnlCustomerrmsg($srchshortcode);
	}
			
  }
		}
	}
	//pledgeTV Code End
	if($cnpformshortcode != ''){
	  $shortcode_tokens = &drupal_static(__FUNCTION__);
	  $cnpresult ="";$mpos="";$cnpgrpname = "";
		foreach ($cnpsplitcntnt as $cntnt) {

		  if (!$cntnt) {
			continue;
		  }
	    
		  if(($cntnt[0] == '[') && (substr($cntnt, -1, 1) == ']')) {
			  $mpos =  array_search("[cnpform]",$cnpsplitcntnt);
			  		if($mpos != ""){$srchshortcode= $cnpsplitcntnt[$mpos] .$cnpsplitcntnt[$mpos+1].$cnpsplitcntnt[$mpos+2];}
			        $chkshortcodexit = $this->CNPDRconnect_isExistShortcode($srchshortcode);

	if($chkshortcodexit)
	{
		 $cnporiginaltext = $srchshortcode; 
		 $mpostn          =  $mpos;
		 $cnpgrpname      = $cnpsplitcntnt[$mpostn+1];
		
		 $cnpformid  = $this->CNPDPCF_getformsofGroup($cnpgrpname);
		 $cnpformtyp = $this->CNPDPCF_getFormType($cnpgrpname);
		
        $cntnt       = substr($cntnt, 1, -1);
        $cntnt       = trim($cntnt);

        $cnpts       = explode(' ', $cntnt);
        $cnptag      = array_shift($cnpts);
        //$cnptag      = trim($cnptag, '/');
		if ($cnptag == "cnpform") {
			 
		if(count($cnpformid) >=1){ 
		$rtrnstrarr = "";
		for($frminc=0;$frminc <	 count($cnpformid);$frminc++)
		{

    	$attrs = array( 'data-guid' => $cnpformid[$frminc]) ;
		$attrs_string = '';
		if(!empty( $attrs ) ) {

			foreach ( $attrs as $key => $value ) {
				$attrs_string .= "$key='" . ( $value ) . "' ";
			}
			$attrs = ltrim( $attrs_string );

	  	 }
		$cnpshortcodearray = explode("--",$cnpformtyp);
    
		if($cnpshortcodearray[0] == 'inline')
		{
		 $rtrnstrarr ='<div class="CnP_inlineform" '.$attrs.'></div>';	
		
		
		}
		else if($cnpshortcodearray[0] == 'popup')
		{
			
			if($cnpshortcodearray[1] == 'text')
			{
				$cnpGetImagesql = $cnpshortcodearray[2];
				$rtrnstrarr.= '<a class="CnP_formlink" data-guid="'.$cnpformid[$frminc].'">'.$cnpGetImagesql.'</a>';
			}
			else if($cnpshortcodearray[1] == 'button')
			{
				$cnpGetbuttontext = $cnpshortcodearray[2];
				$rtrnstrarr.= '<input class="CnP_formlink" type="submit" value="'.$cnpGetbuttontext.'" data-guid="'.$cnpformid[$frminc].'" />';
			}
			else if($cnpshortcodearray[1] == 'image')
			{
			 $cnpGetImage = $cnpshortcodearray[3];
			 $module_handler = \Drupal::service('module_handler');
			 $host = \Drupal::request()->getSchemeAndHttpHost();
			 $module_path = $module_handler->getModule('click_pledge_connect')->getPath()."/banners/".$cnpGetImage;
			 $imagePath=$host.base_path().$module_path;

			 $rtrnstrarr.= '<img class="CnP_formlink " src="'.$imagePath.'" data-guid="'.$cnpformid[$frminc].'" style="cursor: pointer;">';
			}
		}
			 $rtrnstr ="<script>var list = document.getElementsByTagName('script');
				var i = list.length, flag = false;
				while (i--) { 
					if (list[i].src === 'https://resources.connect.clickandpledge.com/Library/iframe-1.0.0.min.js') {
						flag = true;
						break;
					}
				}

				if (!flag) {
					var tag = document.createElement('script');
					
					tag.class ='CnP_formloader';
					tag.src = 'https://resources.connect.clickandpledge.com/Library/iframe-1.0.0.min.js';
					document.getElementsByTagName('body')[0].appendChild(tag);
				}</script>";
			}
	           $output .= $rtrnstr." ".$rtrnstrarr;
			}else{
			 $output  .= $this->CNPDRCF_getGroupCustomerrmsg($cnpgrpname);
	
					}
	     }
	}
	else
	{
	  $output .=$this->CNPDRCF_getGroupCustomerrmsg($cnpgrpname);
	}
  }
 }
}
	 
	    $cnpnewtext = str_replace($cnporiginaltext, $output, $text);
        $result = new FilterProcessResult($cnpnewtext);
    return $result;
	  
  }
public function CNPDPconnect_getGroupchnlCustomerrmsg($cnpshortcode)
{

	$connection= \Drupal::database();
	$prefix=$connection->tablePrefix();
	$cnp_channelgrptable_name    = $prefix.'dp_cnpc_channelgrp';
  $cnpGetImagesql     = "SELECT cnpchannelgrp_custommsg FROM " .$cnp_channelgrptable_name. " WHERE cnpchannelgrp_shortcode ='" .$cnpshortcode. "'";
	$query = $connection->query($cnpGetImagesql);
				$allcnpchannelgrps = $query->fetchAll();
				$cnpformrows = count($allcnpchannelgrps);
	if( $cnpformrows != NULL){
							foreach ($allcnpchannelgrps as $cnpresultsarr) {
							  $cnprtrnstr= $cnpresultsarr->cnpchannelgrp_custommsg;
							 
							}
	}

return $cnprtrnstr;
}
public function CNPDPconnect_getchannelsofGroup($groupname){
	
	
	$connection= \Drupal::database();
	$prefix=$connection->tablePrefix();
	$cnp_channelgrptable_name    = $prefix.'dp_cnpc_channelgrp';
	$cnp_channeltable_name       = $prefix.'dp_cnpc_channeldtl';
	$currentdate       = date("Y-m-d H:i:00");
		 $returnarr = array();
		 $rtrnval="";
	     
		  $scnpSQL    = "SELECT cnpchannelgrp_ID as chnlid FROM ".$cnp_channelgrptable_name ." WHERE cnpchannelgrp_groupname = '".$groupname."' AND 
	 						cnpchannelgrp_status ='active' AND IF (cnpchannelgrp_channel_EndDate !='0000-00-00 00:00:00', '".$currentdate."' between cnpchannelgrp_channel_StartDate and cnpchannelgrp_channel_EndDate, cnpchannelgrp_channel_StartDate <=  '".$currentdate."') order by cnpchannelgrp_Date_Modified DESC Limit 1";
		
				$query = $connection->query($scnpSQL);
				$allcnpchannelgrps = $query->fetchAll();
				$cnpformrows = count($allcnpchannelgrps);
					 if( $cnpformrows != NULL){
							foreach ($allcnpchannelgrps as $cnpresultsarr) {
							  $cnpfrmid= $cnpresultsarr->chnlid;
							 
							}
						   $scnpFormsSQL    = "SELECT cnpchannel_channelName as chnlnm FROM ".$cnp_channeltable_name ." WHERE cnpchannel_cnpchannelgrp_ID = '".$cnpfrmid."' AND cnpchannel_channelStatus ='active' AND   IF (cnpchannel_channelEndDate !='0000-00-00 00:00:00',  '".$currentdate."' between cnpchannel_channelStartDate and cnpchannel_channelEndDate, cnpchannel_channelStartDate <=  '".$currentdate."') order by cnpchannel_DateCreated DESC Limit 1";
						 	$query = $connection->query($scnpFormsSQL);
				$cnpformsresults = $query->fetchAll();
		 
		 $cnpformrows = count($cnpformsresults);;
						
					 if( $cnpformrows != NULL){
							foreach ($cnpformsresults as $cnpfrmresultsarr) {
							
								array_push($returnarr, $cnpfrmresultsarr->chnlnm);
							
							 
							}
						}
						}
				
	return $returnarr;
}
public  function CNPDPconnect_isExistchannelShortcode($cnpshortcode)
{
	$connection= \Drupal::database();
	$prefix=$connection->tablePrefix();
	$cnp_channelgrptable_name    = $prefix.'dp_cnpc_channelgrp';
	$currentdate       = date("Y-m-d H:i:00");
     $cnpGetImagesql     = "SELECT * FROM " .$cnp_channelgrptable_name. " WHERE (cnpchannelgrp_shortcode ='" .$cnpshortcode. "') AND cnpchannelgrp_status ='active' AND IF (cnpchannelgrp_channel_EndDate !='0000-00-00 00:00:00', '".$currentdate."' between cnpchannelgrp_channel_StartDate and cnpchannelgrp_channel_EndDate, cnpchannelgrp_channel_StartDate <= '".$currentdate."') order by cnpchannelgrp_Date_Modified ASC Limit 1";
	$query = $connection->query($cnpGetImagesql);
	$allcnpchannelgrps = $query->fetchAll(); 
	if(count($allcnpchannelgrps) > 0 )return true; else return false;

}
public function CNPDPCF_getFormType($groupname)
{
	$connection= \Drupal::database();
	$prefix=$connection->tablePrefix();
	$cnp_table_name    = $prefix.'dp_cnpc_forminfo';
	$currentdate       = date("Y-m-d H:i:00");
    $cnpGetguidsql     = "SELECT cnpform_type,cnpform_ptype,cnpform_text,cnpform_img FROM " .$cnp_table_name. " WHERE cnpform_groupname ='".$groupname."' AND cnpform_status ='active' AND IF (cnpform_Form_EndDate !='0000-00-00 00:00:00', '".$currentdate."' between cnpform_Form_StartDate and cnpform_Form_EndDate, cnpform_Form_StartDate <= '".$currentdate."') order by cnpform_Date_Modified DESC Limit 1";
	    $cnpfrmcntresultarry       =  $connection->query($cnpGetguidsql);
		$cnpfrmcntresult           =  $cnpfrmcntresultarry->fetchAll();
			foreach ($cnpfrmcntresult as $cnpresultsarr) {
	         $cnpform_accountId = $cnpresultsarr->cnpform_type;
			 return $cnpform_accountId."--".$cnpresultsarr->cnpform_ptype."--".$cnpresultsarr->cnpform_text."--".$cnpresultsarr->cnpform_img;
			}

}
public function CNPDPCF_getformsofGroup($groupname){
	 $returnarr = array();
		 $rtrnval="";
			$connection= \Drupal::database();
			 $prefix=$connection->tablePrefix();
			$cnp_table_name = $prefix.'dp_cnpc_forminfo';
			$cnp_formtable_name =$prefix.'dp_cnpc_formsdtl';
	     $currentdate = date("Y-m-d H:i:00");
		 $scnpSQL    = "SELECT cnpform_GID as frmid FROM ".$cnp_table_name ." WHERE cnpform_groupname = '".$groupname."' AND 
	 						cnpform_status ='active' AND IF (cnpform_Form_EndDate !='0000-00-00 00:00:00', '".$currentdate."' between cnpform_Form_StartDate and cnpform_Form_EndDate, cnpform_Form_StartDate <=  '".$currentdate."') order by cnpform_Date_Modified DESC Limit 1";
			$cnpquery       = $connection->query($scnpSQL);
			$cnpallForms = $cnpquery->fetchAll();
			$cnpformrows = count($cnpallForms);

					 if( $cnpformrows != ""){
							foreach ($cnpallForms as $cnpresultsarr) {
							  $cnpfrmid= $cnpresultsarr->frmid;
							 
							}
						   $scnpFormsSQL    = "SELECT cnpform_GUID as frmguid,cnpform_urlparameters FROM ".$cnp_formtable_name ." WHERE cnpform_cnpform_ID = '".$cnpfrmid."' AND cnpform_FormStatus ='active' AND   IF (cnpform_FormEndDate !='0000-00-00 00:00:00',  '".$currentdate."' between cnpform_FormStartDate and cnpform_FormEndDate, cnpform_FormStartDate <=  '".$currentdate."') order by cnpform_DateCreated DESC Limit 1";
						 $cnpfrmsquery       = $connection->query($scnpFormsSQL);
						 $cnpguid = $cnpfrmsquery->fetchAll();
		   			     $cnpformrows =count($cnpguid);
						
					 if( $cnpformrows != NULL){
							foreach ($cnpguid as $cnpfrmresultsarr) {
								$cnpurlparam = $cnpfrmresultsarr->cnpform_urlparameters;
								$cnpurlparamnw = str_replace(';', '&', $cnpurlparam);
								$newfrmguid = $cnpfrmresultsarr->frmguid ."?".$cnpurlparamnw;
								array_push($returnarr, $newfrmguid);
							
							 
							}
					   }
				  }
				
	return $returnarr;
}
public function CNPDRCF_getGroupCustomerrmsg($cnpshortcode)
{
		$connection= \Drupal::database();
		$prefix=$connection->tablePrefix();
		$cnp_table_name = $prefix.'dp_cnpc_forminfo';
		$cnprtrnstr="";
		$cnpGetImagesql     = "SELECT cnpform_custommsg FROM " .$cnp_table_name. " WHERE cnpform_groupname ='" .$cnpshortcode. "'";
		$cnperrresultqry     =  $connection->query($cnpGetImagesql);
		$cnperrresults       = $cnperrresultqry->fetchAll();
 		if(count($cnperrresults) > 0 )
		   {
			   foreach ($cnperrresults as $cnperrresultsarr)
			   { 
			         $cnprtrnstr= $cnperrresultsarr->cnpform_custommsg;
				
			   }
		   }
return $cnprtrnstr;
}
public function CNPDRconnect_isExistShortcode($cnpshortcode)
{
     $connection = \Drupal::database();
	 $prefix     = $connection->tablePrefix();
	 $table_name1 = $prefix.'dp_cnpc_forminfo';
			
	 $currentdate        = date("Y-m-d H:i:00");
     $cnpGetImagesql     = "SELECT * FROM " .$table_name1. " WHERE (cnpform_shortcode ='" .$cnpshortcode. "') AND cnpform_status ='active' AND IF (cnpform_Form_EndDate !='0000-00-00 00:00:00', '".$currentdate."' between cnpform_Form_StartDate and cnpform_Form_EndDate, cnpform_Form_StartDate <= '".$currentdate."') order by cnpform_Date_Modified ASC Limit 1";
	$query = $connection->query($cnpGetImagesql);
	$allcnpFormsgrps = $query->fetchAll(); 
	if(count($allcnpFormsgrps) > 0 )return true; else return false;

}
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['cnpshortcode_cnpform'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cnpform Shortcode?'),
      '#default_value' => $this->settings['cnpshortcode_cnpform'],
      '#description' => $this->t('Easy to add process for adding Connect forms.'),
    );
	   $form['cnpshortcode_cnppledgeTV'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cnppledgetv Shortcode?'),
      '#default_value' => $this->settings['cnpshortcode_cnppledgeTV'],
      '#description' => $this->t('Easy to add process for adding PledgeTV.'),
    );
    return $form;
  }
}