(function ($, Drupal) {

    'use strict';
    /* CODE GOES HERE */
    // Code that uses jQuery's $ can follow here.
    $("#cnpc-account-settings-form").find("input[type='submit']").val("Verify");
    $(".nickname-div").hide();
    $("#cnpc-account-settings-form").find("input[type='submit']").click(function (e) {
        //alert("Hi");
        if ($("#response_cnpc").val() == "")
        {
            e.preventDefault();
        }
        var accGuid = $("#cnpc_account_guid").val();
        var accNum = $("#cnpc_account_number").val();
        if (accNum.trim() == "")
        {
            $("#cnpc_account_number").css("border", "1px solid red");
            return false;
        }
        if (accGuid.trim() == "")
        {
            $("#cnpc_account_guid").css("border", "1px solid red");
            return false;
        }
        if (accGuid.trim() != "" && accNum.trim() != "")
        {
            var basePath = $("#base_url_cnpc").val()
            var url = location.protocol + "//" + location.host + basePath + "admin/cnpconnect/check/" + accNum + "/" + accGuid;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (res)
                {
                    if (res.status != null)
                    {
                        $(".nickname-div").show()
                        $("#response_cnpc").val(res.status);
                        $("#nickname_cnpc").val(res.status);
                        $("#cnpc-account-settings-form").find("input[type='submit']").val("Save");
                    } else
                    {
                        alert("Unable to find account details");
                        $(".nickname-div").hide();
                        $("#response_cnpc").val("");
                        $("#nickname_cnpc").val("");
                        $("#cnpc-account-settings-form").find("input[type='submit']").val("Verify");
                    }
                },
            });
        }
    });
    $("#cnpc_account_guid").blur(function () {
        if ($(this).val().trim() != "")
        {
            $("#cnpc_account_guid").removeAttr("style");
        }
    });
    $("#cnpc_account_number").blur(function () {
        if ($(this).val().trim() != "")
        {
            $("#cnpc_account_number").removeAttr("style");
        }
    });


    $(document).ready(function () {
        $('#table_id').DataTable();
        $('#table_form_group').DataTable();
        $('#table_viewforms').DataTable();
        
        
        if($("#cnpc_action").val()=="edit")
        {
            $(".cnpcstartdate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });
            });

            $(".cnpcenddate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });
            });
            
            $(".chnlstartdate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });
            });
            
             $(".chnlenddate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });
            });
            $(".cnpc_form_list").each(function(){
                var opttext = $(this).data("opttext");
                var elementID=$(this).attr("id");
                $("#"+elementID+" option").each(function(){
                    if($(this).text()==opttext)
                    {
                        $(this).attr("selected","selected");
                    }
                });
            });
            
            $(".cnpc_chnl_list").each(function(){
                var opttext = $(this).data("opttext");
                var elementID=$(this).attr("id");
                $("#"+elementID+" option").each(function(){
                    if($(this).text()==opttext)
                    {
                        $(this).attr("selected","selected");
                    }
                });
                $("#"+elementID+" option").each(function(){
                    var Otext = $(this).text()+" ("+$(this).val()+")";
                    $(this).text(Otext);
                });
            });
            
            if($('#custrows tr').length-1 >=2)
            {
                $(".deleteRec").show();
            }
            
        }
        $("#start_date_time").datetimepicker({
            timeFormat: 'hh:mm tt',
            dateFormat: 'MM dd, yy',
        });
        $("#end_date_time").datetimepicker({
            timeFormat: 'hh:mm tt',
            dateFormat: 'MM dd, yy',
        });
        /*if($("#cnpc_link_type").val()=="image")
        {
            $("#cnpc_upload_image").show();
            $("#cnpc_btn_lbl").hide();
        }*/
        
        if ($("#cnpc_link_type").val() == "image")
        {
            $(".form-item-files-cnpc-upload-image").show();
            $(".form-item-cnpc-buton-label").hide();
        } else
        {
            $(".form-item-files-cnpc-upload-image").hide();
            $(".form-item-cnpc-buton-label").show();
        }
        //trimming fields
        $('.cnpcaccount-add-form-group input[type="text"]').unbind('blur').bind('blur',function(){
            removeHTMLTags($(this));
            var a = $(this).val().trim();
            $(this).val(a);
        });
        
       $('.cnpc-edit-form-group input[type="text"]').unbind('blur').bind('blur',function(){
           removeHTMLTags($(this));
            var a = $(this).val().trim();
            $(this).val(a);
        });
       
        $('.cnpcaccount-add-channel-group input[type="text"]').unbind('blur').bind('blur',function(){
            removeHTMLTags($(this));
            var a = $(this).val().trim();
            $(this).val(a);
        });
        $('.cnpc-edit-channel-group input[type="text"]').unbind('blur').bind('blur',function(){
            removeHTMLTags($(this));
            var a = $(this).val().trim();
            $(this).val(a);
        });
       $('#cnpc_message').unbind('blur').bind('blur',function(){
            var a = $(this).val().trim();
            $(this).val(a);
        });
        $('#cnpc_account_number').unbind('blur').bind('blur',function(){
            removeHTMLTags($(this));
            var a = $(this).val().trim();
            $(this).val(a);
        });
        $('#cnpc_account_guid').unbind('blur').bind('blur',function(){
            removeHTMLTags($(this));
            var a = $(this).val().trim();
            $(this).val(a);
        });
        //name="cnpc_params_textarea"
         $("textarea[name='cnpc_params_textarea']").unbind('blur').bind('blur',function(){
            removeHTMLTags($(this));
            var a = $(this).val().trim();
            $(this).val(a);
        });
    });

    $("#cnpc_form_group_name").blur(function () {
        $("#cnpc_form_group_name").css("border", "1px solid #b8b8b8");
    });
    
    if($("#cnpc_action").val()=="edit")
    {
        $(".cnpc_settings_form").show();
    }
    else
    {
        $(".cnpc_settings_form").hide();
    }
    
    $("#savebtn").click(function () {
        
        if(validateBasicForm())
        {
            $(".cnpc_settings_form").show();
            getDataFromService($("#cnpc_counter").val());
            var tab = generateTable($("#cnpc_counter").val());
            $("tbody#dynarows").append(tab);

            $(".cnpcstartdate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });
            });
            $(".cnpcenddate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });

            });
            //$("#custrows").after('<a href="javascript:void(0)" onclick="addrows()" id="addrows">Add Form</a>');
            $("#savebtn").hide();
            $("#resetbtn").hide();
            $(".cnpc_hide_show_buttons").show();
            $("#cnpc_counter").val(1);
        }
        return false;
    });
    //add datepicker while edit form loads
   
    
    $("#addrows").click(function (e) {
        e.preventDefault();
        var bPath = $("#base_url_cnpc").val();
        var rowIndex = parseInt($("#cnpc_counter").val());
        var downIndex = rowIndex - 1;
        
        if (cnpcCampListValidate() == false || cnpcFormListValidate() == false || cnpcStartDateValidate() == false || cnpcEndDateValidate() == false)
        {
            return false;
        }


        var rowCount = $('#custrows tr').length;
        
        if ($("#enddate_" + downIndex).val() == "")
        {
            $("#enddate_" + downIndex).next("p").remove();
            $("#enddate_" + downIndex).after("<p class='cnpctext-danger'>End date is required</p>");
            return false;
        }
        if ($("#enddate_" + downIndex).val() != "")
        {
            $("#enddate_" + downIndex).next("p").remove();
        }
        //validateDates($("#start_date_time").val(), $($("#startdate_" + downIndex)).val(),downIndex)
       if(validateDates(downIndex))
       {
            var rows = generateTable(rowIndex);
            $("tbody#dynarows").append(rows);

            $(".cnpcstartdate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });
            });

            $(".cnpcenddate").each(function () {
                $(this).datetimepicker({
                    timeFormat: 'hh:mm tt',
                    dateFormat: 'MM dd, yy',
                });
            });
            if (rowCount >= 2)
            {
                $(".deleteRec").show();
            } else
            {
                $(".deleteRec").hide();
            }
            getDataFromService(rowIndex);
            //console.log("Here");
            $("#cnpc_counter").val(rowIndex + 1);
       }
       else
       {
           console.log("error:"+validateDates(downIndex))
       }
       

        


    });

    /*$(".cnpc_camp_list").live("change",function(){
     console.log($(this));
     alert("hi");
     });*/

    /* $(document).on('click','.cnpcstartdate',function(){
     console.log($(this));
     $(this).datepicker();
     });*/

    $(document).on('change', '.cnpc_camp_list', function () {
        var fi = $(this).data("rowindex");
        //console.log($(this));
        //console.log($("#cnpc_form_list_"+fi).attr("name"));

        var campId = $(this).val();
        var accguid = $("#cnpc_accounts").val();
        var ss = accguid.split("||");
        var AccId = ss[0];
        var AccGuid = ss[1];
        var finalInfo = AccId + "@" + AccGuid + "@" + campId;
        //console.log(campId+"-"+AccId+"-"+AccGuid);
        if ($(this).val() != "")
        {
            $(this).next("p").remove();
        }
        var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/getactivityforms/" + finalInfo;
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function () {
            },
            success: function (res) {

                //console.log($("#cnpc_form_list_"+$(this).data("rowindex")));
                //$("#cnpc_form_list_"+$(this).data("rowindex")).html(res.data);
                $("#cnpc_form_list_" + fi).html(res.data);

                //nsole.log(res.data)
            },
            error: function (err)
            {
                console.log(err)
            }
        });
    });

    $(document).on('change', '.cnpc_form_list', function () {
        //console.log($(this).data("formindex"));
        var indx = $(this).data("formindex");
        var PrevIndx = $(this).data("formindex")-1;
        var PrevEndDt = new Date($("#enddate_"+PrevIndx).val());
        $("#formguid_" + $(this).data("formindex")).val($(this).val());
        if($(this).data("formindex")==0)
        {
            $("#startdate_" + $(this).data("formindex")).val($("#start_date_time").val());
        }
        else
        {           
           $("#startdate_"+indx).datepicker("setDate", addOneDayToDate(PrevEndDt));
        }
        //$("#startdate_" + $(this).data("formindex")).val($("#start_date_time").val());
        if ($(this).val() != "")
        {
            $(this).next("p").remove();
        }
        if ($("#startdate_" + $(this).data("formindex")).val() != "")
        {
            $("#startdate_" + $(this).data("formindex")).next().remove();
        }
    });
    //delete row .deleteRec
    $(document).on('click', '.deleteRec', function (e) {
        //console.log($('#custrows tr').length);
        e.preventDefault();
        var cD = confirm("Are you sure want to delete?");
        if(cD==true)
        {
            var target = $(this).data("target");
            var recid = $(this).data("recid");
            if ($('#custrows tr').length == 3)
            {
                $("#cnpc_counter").val($("#cnpc_counter").val()-$(this).data("index"));
                $("#row_" + $(this).data("index")).remove();
                $(".deleteRec").hide();
                //$("#deleteRec_"+$(this).data("index")).hide();
                //console.log($('#custrows tr:nth-child(2)').attr("id"));
            } else
            {
                $("#row_" + $(this).data("index")).remove();
                $("#cnpc_counter").val($("#cnpc_counter").val()-$(this).data("index"));
            }
            if(target=="edit")
            {
                var recid = $(this).data("recid");
                deleteFormDetails(recid);
            }
        }
    });
    //popupup logic will come here
    $(document).on('click', '.cnpc-myBtn', function (e) {
        e.preventDefault();
        var pos = $(this).data("index");
        var plist = $("#hidden_params_"+pos).val();
       //$(".popupsavebtn").removeAttr("disabled");
       $(".cnpc-popup-success").remove();
        var newid="params_"+pos;
        $(".cnpc-form-group-params textarea").attr("id",newid);
        $(".cnpc-form-group-params textarea").attr("data-params-pos",pos);
        $(".cnpc-form-group-params textarea").val(plist);
        $("#addparams").attr("data-position",pos);
        $("#savecloseparams").attr("data-position",pos);
        $("#cnpc_myModal").show();
    });
    $(".cnpc-close").click(function(){
	$(".cnpc-form-group-params textarea").val("");
	$("#cnpc_myModal").hide();
    });
    $(".popupclosebtn").click(function(e){
        e.preventDefault();
	$(".cnpc-form-group-params textarea").val("");
	$("#cnpc_myModal").hide();
    });
    $(".popupsavebtn").click(function(e){
        e.preventDefault();
        var ppos='';
        $(".cnpc-popup-success").remove();
        ppos = $(this).attr("data-position");
        var listParams = $("#params_"+ppos).val();
        var listParams = listParams.trim();
        if(listParams)
        {
            $("#hidden_params_"+ppos).val(listParams);
            $(".popupsavebtn").after("<span class='cnpc-popup-success'>Parameters added</span>");
        }
       
    });
    
    $(".popupsaveclosebtn").click(function(e){
        e.preventDefault();
        var ppos='';
        $(".cnpc-popup-success").remove();
        ppos = $(this).attr("data-position");
        var listParams = $("#params_"+ppos).val();
        var listParams = listParams.trim();
        $("#hidden_params_"+ppos).val(listParams);
        $(".cnpc-form-group-params textarea").val("");
        $("#cnpc_myModal").hide();
       
    });
    
    /*
    $("textarea[name='cnpc_params_textarea']").keyup(function(){
        if($(this).val().trim()=="")
        {
            $(".popupsavebtn").removeAttr("disabled");
        }
    });
    */
    $("textarea[name='cnpc_params_textarea']").on("keypress",function(e){
        removeHTMLTags($(this));
    });
    function removeHTMLTags(e)
    {
        var val = $("#"+e.attr("id")).val();
        var val =$("#"+e.attr("id")).val();
        var regex = /(<([^>]+)>)/ig;
        var result = val .replace(regex, "");
        $("#"+e.attr("id")).val(result);
       /* var open = val.indexOf('<');
        var close = val.indexOf('>');
        if(open!==-1 && close!==-1) {
          $("#"+e.attr("id")).val(val.replace(val.slice(open,close+1),""));
        }*/
    }
    function deleteFormDetails(id){
        var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/deleteformdetails/" + id;
        $.ajax({
            type: "GET",
            url: url,
            success: function (res) {
                console.log(res)
            },
            error: function (err)
            {
                console.log(err)
            }
        });
    }
    function generateTable(rowIndex)
    {
        //console.log(==1);

        var bPath = $("#base_url_cnpc").val();
        var trow = "";
        trow += "<tr id='row_" + rowIndex + "'>";
        
        trow += "<td>";
        trow += "<select  class='cnpc_camp_list' data-rowindex='" + rowIndex + "'  name='cnpc_camp_list[]' id='cnpc_camp_list_" + rowIndex + "'>";
        trow += "<option value=''>Select Campaign Name</option>";
        trow += "</select><img id='imgloader_" + rowIndex + "' src='" + bPath + "modules/click_pledge_connect/images/loading.gif' height='25' width='25'>";
        trow += "</td>";

        trow += "<td>";
        trow += "<select name='cnpc_form_list[]' data-formindex='" + rowIndex + "' class='cnpc_form_list' id='cnpc_form_list_" + rowIndex + "'>";
        trow += "<option value=''>Select Form Name</option>";
        trow += "</select>";
        trow += "</td>";

        trow += "<td>";
        trow += "<input type='text' name='formguid[]' id='formguid_" + rowIndex + "' disabled='disabled'>";
        trow += "</td>";

        trow += "<td>";
        trow += "<input type='text' name='startdate[]' class='cnpcstartdate' id='startdate_" + rowIndex + "'>";
        trow += "</td>";

        trow += "<td>";
        trow += "<input type='text' name='enddate[]' class='cnpcenddate' id='enddate_" + rowIndex + "'>";
        trow += "</td>";

        trow += "<td id='row_" + rowIndex + "'><input type='hidden' id='hidden_params_"+rowIndex+"' name='hidden_params[]'>";
        trow += "<a href='javascript:void(0)' class='cnpc-myBtn' data-index='" + rowIndex + "'>URL<b>+</b></a> &nbsp; <a title='Delete' href='javascript:void(0)' id='deleteRec_" + rowIndex + "' data-target='add' class='deleteRec' data-index='" + rowIndex + "'><img src='" + bPath + "modules/click_pledge_connect/images/cnpc-trash.png' height='20' width='20'></a>";
        trow += "</td>";
        trow += "</tr>";
        return trow;
    }

    function hiddenFormValidate()
    {
        var inps1 = document.getElementsByName('cnpc_camp_list[]');
        var stopper = true;
        for (var k = 0; k < inps1.length; k++)
        {
            //console.log(inps1[k].value);
            if (inps1[k].value == "")
            {
                alert("select Campaign list");
                stopper = false;
            }
        }
        return stopper;
    }

    function cnpcCampListValidate()
    {
        var stopper = true;
        if($("#cnpc_action").val()=="edit")
        {
            $("select[name^='cnpc_camp_list_']" ).each(function(){
                if($(this).val()=="")
                {
                    $("#" + $(this).attr("id")).next().remove();
                    $("#" + $(this).attr("id")).after("<p class='cnpctext-danger'>Please select a campaign</p>");
                    stopper = false;
                }
            });
            
        }
        var inps1 = document.getElementsByName('cnpc_camp_list[]');

        for (var k = 0; k < inps1.length; k++)
        {
            //console.log(inps1[k].value);
            if (inps1[k].value == "")
            {
                $("#" + inps1[k].id).next().remove();
                $("#" + inps1[k].id).after("<p class='cnpctext-danger'>Please select a campaign</p>");
                stopper = false;
                break;
            }
        }
        
        return stopper;
    }
    function cnpcFormListValidate()
    {
        var stopper = true;
        if($("#cnpc_action").val()=="edit")
        {
            $("select[name^='cnpc_form_list_']" ).each(function(){
                if($(this).val()=="")
                {
                    $("#" + $(this).attr("id")).next().remove();
                    $("#" + $(this).attr("id")).after("<p class='cnpctext-danger'>Please select a form</p>");
                    stopper = false;
                }
            });
            
        }
        var inps1 = document.getElementsByName('cnpc_form_list[]');
        
        for (var k = 0; k < inps1.length; k++)
        {
            //console.log(inps1[k].value);
            if (inps1[k].value == "")
            {
                $("#" + inps1[k].id).next().remove();
                $("#" + inps1[k].id).after("<p class='cnpctext-danger'>Please select a form</p>");
                stopper = false;
                break;
            }
        }
        return stopper;
    }

    function cnpcStartDateValidate()
    {
        var stopper = true;
        if($("#cnpc_action").val()=="edit")
        {
            $("select[name^='startdate_']" ).each(function(){
                if($(this).val()=="")
                {
                    $("#" + $(this).attr("id")).next().remove();
                    $("#" + $(this).attr("id")).after("<p class='cnpctext-danger'>Please select a start date</p>");
                    stopper = false;
                }
            });
            
        }
        var inps1 = document.getElementsByName('startdate[]');
       
        for (var k = 0; k < inps1.length; k++)
        {
            //console.log(inps1[k].value);
            if (inps1[k].value == "")
            {
                $("#" + inps1[k].id).next().remove();
                $("#" + inps1[k].id).after("<p class='cnpctext-danger'>Please select a start date</p>");
                stopper = false;
                break;
            }
        }
        return stopper;
    }
    function cnpcEndDateValidate()
    {
         var stopper = true;
        if($("#cnpc_action").val()=="edit")
        {
            $("select[name^='enddate_']" ).each(function(){
                if($(this).val()=="")
                {
                    $("#" + $(this).attr("id")).next().remove();
                    $("#" + $(this).attr("id")).after("<p class='cnpctext-danger'>Please select a end date</p>");
                    stopper = false;
                }
            });
            
        }
        var inps1 = document.getElementsByName('enddate[]');
        for (var k = 0; k < inps1.length; k++)
        {
            
            if (k != 0)
            {
                if (inps1[k].value == "")
                {
                    if(k==(inps1.length-1))
                    {
                        stopper = true;
                    }
                    else
                    {
                        $("#" + inps1[k].id).next().remove();
                        $("#" + inps1[k].id).after("<p class='cnpctext-danger'>Please select a end date</p>");
                        stopper = false;
                        break;
                    }
                }
            }
        }
        return stopper;
    }

    function getDataFromService(ind)
    {
        //console.log("here1");
        var accguid = $("#cnpc_accounts").val();
        var obj = accguid.split("||");
        let accid = obj[0];
        let guid = obj[1];
        let finalguid = accid + "@" + guid;
        //console.log(accid+"-"+guid);
        //fire ajax call to get the campaign name based on accid and guid
        var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/getcampaigns/" + finalguid;
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function () {
                $("#imgloader_" + ind).show();
            },
            success: function (res) {
                //console.log(res.data);
                //console.log("#cnpc_camp_list_"+ind);
                $("#cnpc_camp_list_" + ind).html(res.data);
                $("#imgloader_" + ind).hide();
            },
            error: function (err)
            {

            }
        });
    }
    $("#finalsave").click(function () {
       
       if(!validateBasicForm())
       {
           return false;
       }
        var  stdt  = $("#start_date_time").val();
        var  eddt  = $("#end_date_time").val();
        var greg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        if (stdt.match(greg))
        {
            var gres  = stdt.split(" ");
            var gres1 = gres[0].split("/");
            var gres2 = gres1[1]+"/"+gres1[0]+"/"+gres1[2];
            stdt = gres2 + " "+gres[1]+ " "+gres[2];
        }
        if(eddt !="")
        {
            if (eddt.match(greg))
            {
                var gcres  = eddt.split(" ");
                var gcres1 = gcres[0].split("/");
                var gcres2 = gcres1[1]+"/"+gcres1[0]+"/"+gcres1[2];
                eddt       = gcres2 + " "+gcres[1]+ " "+gcres[2];
           }
        }
       
        var  mstdt = new Date(stdt);
        var  meddt = new Date(eddt);
        
        var  campfldnm;
        var  frmfldnm;
        var  guidfldnm;
        var  chksdt   = $("#start_date_time").val();
        var  chkedt   = $("#end_date_time").val()
  
        var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        if (chksdt.match(reg))
        {
            var res = chksdt.split(" ");
            var res1 = res[0].split("/");
            var res2 = res1[1]+"/"+res1[0]+"/"+res1[2];
            chksdt = res2 + " "+res[1]+ " "+res[2];
        }
	if(chkedt !="")
        { 
            if(chkedt.match(reg))
            {
                var cres = chkedt.split(" ");
                var cres1 = cres[0].split("/");
                var cres2 = cres1[1]+"/"+cres1[0]+"/"+cres1[2];
                chkedt = cres2 + " "+cres[1]+ " "+cres[2];
            }
        }
        var  mchksdt  =  new Date(chksdt);
        var  mchkedt  =  new Date(chkedt);
       
        if($("#start_date_time").val() == "")
        {
            $("#start_date_time").addClass( "cnpcform-invalid" );
            $("#start_date_time").focus();
            return false;
        }
         if(chksdt != "" && chkedt != ""){
            if(mchksdt.getTime() > mchkedt.getTime())
            {
                $("#end_date_time").addClass( "cnpcform-invalid" );
                $("#end_date_time").focus();
                return false;
            }
        }
        //display type validation
        if($("#cnpc_display_type").val() == "")
        {
            $("#cnpc_display_type").addClass( "cnpcform-invalid" );
            $("#cnpc_display_type").focus();
            return false;
        }
        if($("#cnpc_display_type").val() == "popup" && $("#cnpc_link_type").val() == "")
        {
            $("#cnpc_link_type" ).addClass( "cnpcform-invalid" );
            return false;
        }
         if($("#cnpc_display_type").val() == "popup" && ($("#cnpc_display_type").val() == "text" || $("#cnpc_display_type").val() == "button") && $("#cnpc_btn_lbl").val() == "")
        {
            $("#cnpc_btn_lbl" ).addClass( "cnpcform-invalid" );
            return false;
        }

       var form_data = new FormData();
        
        
        var formAction = $("#cnpc_action").val();
        var camp = cnpcCampListValidate();
        var form = cnpcFormListValidate();
        var sdate = cnpcStartDateValidate();
        var edate = cnpcEndDateValidate();
        
        var nofrms = $("#cnpc_counter").val();
        nofrms = nofrms-1;
        for(var j=0;j<=nofrms;j++)
        {
           console.log("Here")
            var  strtdt1 = $("#startdate_"+j).val();
            var  chkedt1 = $("#enddate_"+j).val();
           // console.log(chkedt1+"=="+strtdt1);
            var ereg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
            if (strtdt1.match(ereg))
            {
                var eres = strtdt1.split(" ");
                var eres1 = eres[0].split("/");
                var eres2 = eres1[1]+"/"+eres1[0]+"/"+eres1[2];
                strtdt1 = eres2 + " "+eres[1]+ " "+eres[2];
            }
            if(chkedt1 !="")
            {
                if (chkedt1.match(ereg))
                {
                    var ecres  = chkedt1.split(" ");
                    var ecres1 = ecres[0].split("/");
                    var ecres2 = ecres1[1]+"/"+ecres1[0]+"/"+ecres1[2];
                    chkedt1    = ecres2 + " "+ecres[1]+ " "+ecres[2];
                }
            }
            var  mchksdt1  =  new Date(strtdt1);
            var  mchkedt1  =  new Date(chkedt1);
            if(strtdt1 != "" && chkedt1 == ""){
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    $("#startdate_"+j).next().remove();
                    $("#startdate_"+j).after("<p class='cnpc-danger'>Form start date should be greater or equal to group start date</p>");
                    $("#startdate_"+j).focus();
                    return false;
                }
                if(mchksdt1.getTime() > meddt.getTime() )
                {
                    $("#startdate_"+j).next().remove();
                    $("#startdate_"+j).after("<p class='cnpc-danger'>Form start date should be less than or equal to group end date</p>");
                    $("#startdate_"+j).focus();
                    return false;
                }
            }    
            if(strtdt1 != "" && chkedt1 != ""){
                if(mchkedt1.getTime() > meddt.getTime() )
                {
                    $("#enddate_"+j).next().remove();
                    $("#enddate_"+j).after("<p class='cnpc-danger'>Form end date should be less than or equal to group end date</p>");
                    $("#enddate_"+j).focus();
                    return false;
                }
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    $("#startdate_"+j).next().remove();
                    $("#startdate_"+j).after("<p class='cnpc-danger'>Form start date should be greater or equal to group start date</p>");
                    $("#startdate_"+j).focus();
                    return false;
                }
                if(mchksdt1.getTime() > mchkedt1.getTime())
                {   
                    $("#enddate_"+j).next().remove();
                    $("#enddate_"+j).after("<p class='cnpc-danger'>End date should be greater than or equal to start date</p>");
                    $("#enddate_"+j).focus();
                    return false;
                }
            }       
        }
        
        if (camp && form && sdate && edate)
        {
            
            var cnpcfgn = $("#cnpc_form_group_name").val();
            var cnpcacc = $("#cnpc_accounts").val();
            var cnpcsdt = $("#start_date_time").val();
            var cnpcedt = $("#end_date_time").val();
            var cnpcdtype = $("#cnpc_display_type").val();
            
            form_data.append('cnpc_action', formAction);
            form_data.append('cnpc_group_name', cnpcfgn);
            form_data.append('cnpc_account', cnpcacc);
            form_data.append('cnpc_start_datetime', cnpcsdt);
            form_data.append('cnpc_end_datetime', cnpcedt);
            form_data.append('cnpc_display_type', cnpcdtype);
            
            if(cnpcdtype=="popup")
            {
                var cnpcltype = $("#cnpc_link_type").val();
                form_data.append('link_type', cnpcltype);
                if(cnpcltype=="image")
                {
                    var filepath= $("#cnpc_upload_image").val();
                    //if file uploads, move the file into server location
                    var files = document.getElementById('cnpc_upload_image').files;
                    if(files.length!=0)
                    {
                        if(files[0].type=="image/png" || files[0].type=="image/jpg" || files[0].type=="image/jpeg")
                        {
                            form_data.append('file', files[0]); 
                            //console.log(files[0].type);
                        }
                        else
                        {
                            alert("Please select a valid image(.png, .jpg)");
                            $("#cnpc_upload_image").focus();
                            return false;
                        }
                    }
                    else
                    {
                        if($("#cnpc_action").val()=="edit")
                        {
                            var hiddenImg = $("#cnpc_upload_image_hidden").val();
                            form_data.append('file', hiddenImg); 
                        }
                        else
                        {
                            alert("Please select an image");
                            $("#cnpc_upload_image").focus();
                            return false;
                        }
                    }
                }
                else
                {
                    var btnlbl = $("#cnpc_btn_lbl").val();
                    var linktype = $("#cnpc_link_type").val();
                    form_data.append('link_type', linktype);
                    
                    if(btnlbl != "")
                    {
                        form_data.append('button_label', btnlbl);
                    }
                    else
                    {
                        alert("Please enter button label");
                        $("#cnpc_btn_lbl").focus();
                        return false;
                    }
                }
            }
            var cmpcmsg = $("#cnpc_message").val();
            var status = $("#cnpc_status").val();
            
            form_data.append('cnpc_message', cmpcmsg);
            form_data.append('cnpc_status', status);
            var basePath = $("#base_url_cnpc").val();
            var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/saveformdata/";
            
            //collect the data from groups
            if(formAction=="add")
            {
                var camp_list = getValues("cnpc_camp_list[]");
                var form_list = getValues("cnpc_form_list[]");
                var form_list_titles = getTitles("cnpc_form_list[]");
                var formguid = getValues("formguid[]");
                var startdate = getValues("startdate[]");
                var enddate = getValues("enddate[]");
                var paramsList = getValues("hidden_params[]");
            }
            else
            {
                var camp_list = getFormSelectValues("cnpc_camp_list");
                var form_list = getFormSelectValues("cnpc_form_list");
                var form_list_titles = getSelectTitles("cnpc_form_list");
                var formguid = getFormValues("formguid");
                var startdate = getFormValues("startdate");
                var enddate = getFormValues("enddate");
                var cnpc_edit_id = $("#cnpc_edit_id").val();
                var paramsList = getFormValues("hidden_params");
                form_data.append("cnpc_edit_id",cnpc_edit_id);
            }
            //console.log(paramsList);
            //return false;
            form_data.append("cnpc_camp_list",camp_list);
            form_data.append("cnpc_form_list",form_list);
            form_data.append("formguid",formguid);
            form_data.append("startdate",startdate);
            form_data.append("enddate",enddate);
            form_data.append("form_list_titles",form_list_titles);
            form_data.append("params_list",paramsList);
            //console.log(form_list_titles)
            //console.log($("#cnpc_action").val());
           
            $.ajax({
                url:url,
                data:form_data,
                cache : false,
                contentType: false,
                method:'POST',
                processData : false,
                dataType    : 'json',
                success:function(res){
                    console.log(res.data);
                    if(res.data=="success")
                    {
                        if(formAction=="edit")
                        {
                            window.location="../cnp_form";
                        }
                        else
                        {
                            window.location = "cnp_form";
                        }
                    }
                    //updateForms(form_gdata,res.data);
                    return false;
                },
            });
            
        }

        return false;
    });
    
    function getValues(name){
        var fdata="";
        var inps = document.getElementsByName(name);
        for (var i = 0; i <inps.length; i++) {
            var inp=inps[i];
            fdata += inp.value+"###";
        }
        return fdata;
    }
    function getFormValues(name)
    {
        var fvdata='';
        $("input[name^='"+name+"']" ).each(function(){
            fvdata += $("#"+$(this).attr("id")).val()+"###";
        })
        return fvdata;
    }
    function getFormSelectValues(name)
    {
        var fsdata='';
        $("select[name^='"+name+"']" ).each(function(){
           fsdata += $("#"+$(this).attr("id")).val()+"###";
        })
        return fsdata;
    }
    function getSelectTitles(name)
    {
        var ftitles="";
         $("select[name^='"+name+"']" ).each(function(){
             ftitles += $('#'+$(this).attr("id")+' option:selected').text()+"###";
         });
         return ftitles;
    }
    function getTitles(name){
        var fdata="";
        var inps = document.getElementsByName(name);
        for (var i = 0; i <inps.length; i++) {
            var inp=inps[i];
            fdata += $('#'+inp.id+' option:selected').text()+"###";
        }
        return fdata;
    }
    
    function updateForms(form_gdata,lastID)
    {
        console.log(form_gdata)
    }
    
    function getCurrentDate()
    {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var d = new Date();
        var n = d.getMonth();
        var dt = monthNames[d.getMonth()];
        var day = d.getDate();
        if (day <= 9)
        {
            day = "0" + day;
        }
        return dt + " " + day + ", " + d.getFullYear() + " " + formatAMPM(new Date);
        ;
    }
    function addOneDayToDate(dateStr)
    {
        const monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
        ];
        var days = 1;
        var result = new Date(new Date(dateStr).setDate(new Date(dateStr).getDate() + days));
        var d = result;
        var n = d.getMonth();
        var dt = monthNames[d.getMonth()];
        var day = d.getDate();
        if (day <= 9)
        {
                day = "0" + day;
        }
        return dt + " " + day + ", " + d.getFullYear() + " " + formatAMPM(dateStr);
    }
    function formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }
    if($("#cnpc_display_type").val()=="popup")
    {
        $(".overlay_wrapper").show();
    }
    else
    {
        $(".overlay_wrapper").hide();
    }
    $("#cnpc_display_type").change(function () {
        if($(this).val() == "popup")
        {
            $(".overlay_wrapper").show();
        } else
        {
            $(".overlay_wrapper").hide();
        }
    });

    $("#cnpc_link_type").change(function () {
        if ($(this).val() == "image")
        {
            $(".form-item-files-cnpc-upload-image").show();
            $(".form-item-cnpc-buton-label").hide();
        } else
        {
            $(".form-item-files-cnpc-upload-image").hide();
            $(".form-item-cnpc-buton-label").show();
        }
    });
    function validateDates(j)
    {
        //console.log(j);
        var status=true;
        var  strtdt1 = $("#startdate_"+j).val();
        var  chkedt1 = $("#enddate_"+j).val();
       //console.log($("#startdate_"+j));
        var  stdt  = $("#start_date_time").val();
        var  eddt  = $("#end_date_time").val();
        var greg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        if (stdt.match(greg))
        {
            var gres  = stdt.split(" ");
            var gres1 = gres[0].split("/");
            var gres2 = gres1[1]+"/"+gres1[0]+"/"+gres1[2];
            stdt = gres2 + " "+gres[1]+ " "+gres[2];
        }
        if(eddt !="")
        {
            if (eddt.match(greg))
            {
                var gcres  = eddt.split(" ");
                var gcres1 = gcres[0].split("/");
                var gcres2 = gcres1[1]+"/"+gcres1[0]+"/"+gcres1[2];
                eddt       = gcres2 + " "+gcres[1]+ " "+gcres[2];
           }
        }
       
        var  mstdt = new Date(stdt);
        var  meddt = new Date(eddt);
            var ereg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
            if (strtdt1.match(ereg))
            {
                var eres = strtdt1.split(" ");
                var eres1 = eres[0].split("/");
                var eres2 = eres1[1]+"/"+eres1[0]+"/"+eres1[2];
                strtdt1 = eres2 + " "+eres[1]+ " "+eres[2];
            }
            if(chkedt1 !="")
            {
                if (chkedt1.match(ereg))
                {
                    var ecres  = chkedt1.split(" ");
                    var ecres1 = ecres[0].split("/");
                    var ecres2 = ecres1[1]+"/"+ecres1[0]+"/"+ecres1[2];
                    chkedt1    = ecres2 + " "+ecres[1]+ " "+ecres[2];
                }
            }
            var  mchksdt1  =  new Date(strtdt1);
            var  mchkedt1  =  new Date(chkedt1);
            if(strtdt1 != "" && chkedt1 == ""){
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    $("#startdate_"+j).next().remove();
                    $("#startdate_"+j).after("<p class='cnpc-danger'>Form start date should be greater or equal to group start date</p>");
                    $("#startdate_"+j).focus();
                    status = false;
                }
                if(mchksdt1.getTime() > meddt.getTime() )
                {
                    $("#startdate_"+j).next().remove();
                    $("#startdate_"+j).after("<p class='cnpc-danger'>Form start date should be less than or equal to group end date</p>");
                    $("#startdate_"+j).focus();
                    status = false;
                }
            }    
            if(strtdt1 != "" && chkedt1 != ""){
                if(mchkedt1.getTime() > meddt.getTime() )
                {
                    $("#enddate_"+j).next().remove();
                    $("#enddate_"+j).after("<p class='cnpc-danger'>Form end date should be less than or equal to group end date</p>");
                    $("#enddate_"+j).focus();
                    status = false;
                }
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    $("#startdate_"+j).next().remove();
                    $("#startdate_"+j).after("<p class='cnpc-danger'>Form start date should be greater or equal to group start date</p>");
                    $("#startdate_"+j).focus();
                    status = false;
                }
                if(mchksdt1.getTime() > mchkedt1.getTime())
                {   
                    $("#enddate_"+j).next().remove();
                    $("#enddate_"+j).after("<p class='cnpc-danger'>End date should be greater than or equal to start date</p>");
                    $("#enddate_"+j).focus();
                    status = false;
                }
            }
            return status;
    }
    
    $(".formstatus").click(function(event){
        event.preventDefault();
       var elemID = $(this).attr("id");
         var fstatus;
        if($("#"+elemID).data("fstatus")=="active")
        {
           fstatus="inactive";
        }
        else
        {
           fstatus="active";
        }
         console.log(fstatus);
        
        var gid = $(this).data("fgid");
        var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/updateFormStatus/"+fstatus+"/"+gid;
            
        $.ajax({
            url:url,
            type: "GET",
            success:function(res){
                if(res.data=="success")
                {
                    $("#"+elemID).html(fstatus);
                    $("#"+elemID).attr("data-fstatus",fstatus);
                }
            },
            error:function(err){
                   console.log(err);
            },
            });
    });
    
    $(".ptvformstatus").click(function(event){
        event.preventDefault();
       var elemID = $(this).attr("id");
         var fstatus;
        if($("#"+elemID).data("fstatus")=="active")
        {
           fstatus="inactive";
        }
        else
        {
           fstatus="active";
        }
         console.log(fstatus);
        
        var gid = $(this).data("fgid");
        var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/updatepTVFormStatus/"+fstatus+"/"+gid;
            
        $.ajax({
            url:url,
            type: "GET",
            success:function(res){
                if(res.data=="success")
                {
                    $("#"+elemID).html(fstatus);
                    $("#"+elemID).attr("data-fstatus",fstatus);
                }
            },
            error:function(err){
                   console.log(err);
            },
            });
    });
    
    
    $(".deleteFormInfo").click(function(e){
        e.preventDefault();
        var did = $(this).data("recid");
         var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/deleteform/"+did;
            
        $.ajax({
            url:url,
            type: "GET",
            success:function(res){
                if(res.data=="success")
                {
                    window.location="cnp_form";
                }
                else
                {
                    window.location="cnp_form";
                }
            },
            error:function(err){
                   console.log(err);
            },
        });
    });
    $(".settingsDel").click(function(e){
        e.preventDefault();
        var setdid = $(this).data("settrecid");
        var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/deletesettings/"+setdid;
        $.ajax({
            url:url,
            type: "GET",
            success:function(res){
                if(res.data=="success")
                {
                    var date = new Date();
                    date.setTime(date.getTime() + (3 * 1000));
                    $.cookie('settingsdel',"Account Deleted Successfully", { expires: date });
                    window.location="cnp_formssettings";
                }
                else
                {
                    $(".cnpc_error_msg").show();
                    $(".cnpc_error_msg").addClass("cnpc_error");
                    $(".cnpc_error_msg").html("This Account Number is associated with an existing form group. You must first delete the form group before deleting this account.");
                }
            },
            error:function(err){
               console.log("unable to process");
            },
        });    
    });
    function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
    }
    //settingsdel
    if(readCookie("settingsdel"))
    {
        //console.log(readCookie("settingsdel"))
        $(".cnpc_error_msg").show();
        $(".cnpc_error_msg").focus();
        $(".cnpc_error_msg").addClass("cnpc_success");
        $(".cnpc_error_msg").html(decodeURI(readCookie("settingsdel")));
    }
    
    $(".refreshAccounts").click(function(e){
        e.preventDefault();
       var check = confirm("I am refreshing the account information, please wait");
       if(check==true)
       {
            var settguid = $(this).data('settguid'); 
            var settaccid = $(this).data('settaccid'); 
            var recid = $(this).data('settid'); 
            var basePath = $("#base_url_cnpc").val();
            var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/refreshaccounts/";
            $.ajax({
                url:url,
                type: "POST",
                data:{sguid:settguid,saccid:settaccid,srecid:recid},
                success:function(res){
                    console.log(res);
                    if(res.data=="success")
                    {
                           alert("All account information has been updated.  Please click OK to continue.");
                           window.location="cnp_formssettings";
                    }
                    else
                    {
                        //alert("Sorry! Unable to process.Try again");
                        alert("All account information has been updated.  Please click OK to continue.");
                        window.location="cnp_formssettings";
                    }
                },
                error:function(err){
                   console.log("unable to process");
                },
            });    
       }
       
    });
    /**************************************************************
     * **************PledgeTV JS Starts Here ********************
     ****************************************************************/
    $("#addchannelrows").click(function (e) {
        e.preventDefault();
        var bPath = $("#base_url_cnpc").val();
        if (cnpcChnlListValidate() == false  || cnpcchnlStartDateValidate() == false || cnpcchnlEndDateValidate() == false)
        {
            return false;
        }


        var crowCount = $('#custchnlrows tr').length; 
        var crowIndex = parseInt($("#cnpcchnl_counter").val());
        var cdownIndex = crowIndex - 1;
        if ($("#chnlenddate_" + cdownIndex).val() == "")
        {
            $("#chnlenddate_" + cdownIndex).next("p").remove();
            $("#chnlenddate_" + cdownIndex).after("<p class='cnpctext-danger'>End date is required</p>");
            return false;
        }
        if ($("#chnlenddate_" + cdownIndex).val() != "")
        {
            $("#chnlenddate_" + cdownIndex).next("p").remove();
        }
        if(validateChannelDates(cdownIndex))
        {
            var crows = generateChannelTable(crowIndex);
          $("tbody#dynachnlrows").append(crows);
        var chksdt=$("#chnlenddate_"+cdownIndex).val();
             
        $(".cnpcstartdate").each(function () {
            $(this).datetimepicker({
                timeFormat: 'hh:mm tt',
                dateFormat: 'MM dd, yy',
            });
        });
        $(".cnpcenddate").each(function () {
            $(this).datetimepicker({
                timeFormat: 'hh:mm tt',
                dateFormat: 'MM dd, yy',
            });
        });
        if (crowCount >= 2)
        {
            $(".deleteRec").show();
        } else
        {
            $(".deleteRec").hide();
        }
        getDataChannelService(crowIndex);
     
        $("#cnpcchnl_counter").val(crowIndex + 1);
        }
        else
        {
            
        }

    });
    
    function validateChannelDates(j)
    {
        //console.log(j);
        var status=true;
        var  strtdt1 = $("#chnlstartdate_"+j).val();
        var  chkedt1 = $("#chnlenddate_"+j).val();
       //console.log($("#startdate_"+j));
        var  stdt  = $("#start_date_time").val();
        var  eddt  = $("#end_date_time").val();
        var greg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        if (stdt.match(greg))
        {
            var gres  = stdt.split(" ");
            var gres1 = gres[0].split("/");
            var gres2 = gres1[1]+"/"+gres1[0]+"/"+gres1[2];
            stdt = gres2 + " "+gres[1]+ " "+gres[2];
        }
        if(eddt !="")
        {
            if (eddt.match(greg))
            {
                var gcres  = eddt.split(" ");
                var gcres1 = gcres[0].split("/");
                var gcres2 = gcres1[1]+"/"+gcres1[0]+"/"+gcres1[2];
                eddt       = gcres2 + " "+gcres[1]+ " "+gcres[2];
           }
        }
       
        var  mstdt = new Date(stdt);
        var  meddt = new Date(eddt);
            var ereg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
            if (strtdt1.match(ereg))
            {
                var eres = strtdt1.split(" ");
                var eres1 = eres[0].split("/");
                var eres2 = eres1[1]+"/"+eres1[0]+"/"+eres1[2];
                strtdt1 = eres2 + " "+eres[1]+ " "+eres[2];
            }
            if(chkedt1 !="")
            {
                if (chkedt1.match(ereg))
                {
                    var ecres  = chkedt1.split(" ");
                    var ecres1 = ecres[0].split("/");
                    var ecres2 = ecres1[1]+"/"+ecres1[0]+"/"+ecres1[2];
                    chkedt1    = ecres2 + " "+ecres[1]+ " "+ecres[2];
                }
            }
            var  mchksdt1  =  new Date(strtdt1);
            var  mchkedt1  =  new Date(chkedt1);
            if(strtdt1 != "" && chkedt1 == ""){
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    $("#chnlstartdate_"+j).next().remove();
                    $("#chnlstartdate_"+j).after("<p class='cnpc-danger'>Form start date should be greater or equal to group start date</p>");
                    $("#chnlstartdate_"+j).focus();
                    status = false;
                }
                if(mchksdt1.getTime() > meddt.getTime() )
                {
                    $("#chnlstartdate_"+j).next().remove();
                    $("#chnlstartdate_"+j).after("<p class='cnpc-danger'>Form start date should be less than or equal to group end date</p>");
                    $("#chnlstartdate_"+j).focus();
                    status = false;
                }
            }    
            if(strtdt1 != "" && chkedt1 != ""){
                if(mchkedt1.getTime() > meddt.getTime() )
                {
                    $("#chnlenddate_"+j).next().remove();
                    $("#chnlenddate_"+j).after("<p class='cnpc-danger'>Form end date should be less than or equal to group end date</p>");
                    $("#chnlenddate_"+j).focus();
                    status = false;
                }
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    $("#chnlstartdate_"+j).next().remove();
                    $("#chnlstartdate_"+j).after("<p class='cnpc-danger'>Form start date should be greater or equal to group start date</p>");
                    $("#chnlstartdate_"+j).focus();
                    status = false;
                }
                if(mchksdt1.getTime() > mchkedt1.getTime())
                {   
                    $("#chnlenddate_"+j).next().remove();
                    $("#chnlenddate_"+j).after("<p class='cnpc-danger'>End date should be greater than or equal to start date</p>");
                    $("#chnlenddate_"+j).focus();
                    status = false;
                }
            }
            return status;
    }
    
    
    function cnpcChnlListValidate()
    {
       
        var stopper = true;
        if($("#cnpc_action").val()=="edit")
        {
            
             $("select[name^='cnpc_chnl_list_']" ).each(function(){
                if($(this).val()=="")
                {
                     console.log("In In Here");
                    $("#" + $(this).attr("id")).next().remove();
                    $("#" + $(this).attr("id")).after("<p class='cnpctext-danger'>Please select a channel</p>");
                    stopper = false;
                }
               
            });
        }
        var inps1 = document.getElementsByName('cnpc_chnl_list[]');
        for (var k = 0; k < inps1.length; k++)
        {
          
            if (inps1[k].value == "")
            {
                $("#" + inps1[k].id).next().remove();
                $("#" + inps1[k].id).after("<p class='cnpctext-danger'>Please select a channel</p>");
                stopper = false;
                break;
            }
        }
        return stopper;
    }
    function cnpcchnlStartDateValidate()
    {
        var stopper = true;
        if($("#cnpc_action").val()=="edit")
        {
             $("input[name^='chnlstartdate_']" ).each(function(){
                if($(this).val()=="")
                {
                    $("#" + $(this).attr("id")).next().remove();
                    $("#" + $(this).attr("id")).after("<p class='cnpctext-danger'>Please select a start date</p>");
                    stopper = false;
                }
            });
        }
        var inps1 = document.getElementsByName('chnlstartdate[]');
        for (var k = 0; k < inps1.length; k++)
        {
            //console.log(inps1[k].value);
            if (inps1[k].value == "")
            {
                $("#" + inps1[k].id).next().remove();
                $("#" + inps1[k].id).after("<p class='cnpctext-danger'>Please select a start date</p>");
                stopper = false;
                break;
            }
        }
        return stopper;
    }
    function cnpcchnlEndDateValidate()
    {
        var stopper = true;
        if($("#cnpc_action").val()=="edit")
        {
             $("input[name^='chnlenddate_']" ).each(function(){
                if($(this).val()=="")
                {
                    $("#" + $(this).attr("id")).next().remove();
                    $("#" + $(this).attr("id")).after("<p class='cnpctext-danger'>Please select a end date</p>");
                    stopper = false;
                }
            });
        }
        var inps1 = document.getElementsByName('chnlenddate[]');
        for (var k = 0; k < inps1.length; k++)
        {
            
            if (k != 0)
            {
                if (inps1[k].value == "")
                {
                    if(k==(inps1.length-1))
                    {
                        stopper = true;
                    }
                    else
                    {
                        $("#" + inps1[k].id).next().remove();
                        $("#" + inps1[k].id).after("<p class='cnpctext-danger'>Please select a end date</p>");
                        stopper = false;
                        break;
                    }
                }
            }
        }
        return stopper;
    }
    function getDataChannelService(ind)
    {
        
        var accguid = $("#cnpc_accounts").val();
        var obj = accguid.split("||");
        let accid = obj[0];
        let guid = obj[1];
        let finalguid = accid + "@" + guid;
       
        var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/getchannels/" + finalguid;
       
        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function () {
                $("#imgloader_" + ind).show();
            },
            success: function (res) {
           
                $("#cnpc_chnl_list_" + ind).html(res.data);
                $("#imgloader_" + ind).hide();
            },
            error: function (err)
            {
                console.error(err);
            }
        });
    }
    $("#finalchnlsave").click(function () {
       
     
    if ($("#cnpc_channel_group_name").val() == "")
    {
        $("#cnpc_channel_group_name").css("border", "1px solid red");
        $("#cnpc_channel_group_name").focus();
        return false;
    }
    else
    {
        $("#cnpc_channel_group_name").css("border", "1px"); 
    }
    if($("#start_date_time").val() == "")
    {
        $("#start_date_time").css("border", "1px solid red");
        $("#start_date_time").focus();
        return false;
    }
     
     var  stdt  = $("#start_date_time").val();
     var  eddt  = $("#end_date_time").val();
     var  greg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
       
       
        var  mstdt = new Date(stdt);
        var  meddt = new Date(eddt);
        /*****************************************/
        
        var  chksdt   = $("#start_date_time").val();
        var  chkedt   = $("#end_date_time").val()
  
        var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        if (chksdt.match(reg))
        {
            var res = chksdt.split(" ");
            var res1 = res[0].split("/");
            var res2 = res1[1]+"/"+res1[0]+"/"+res1[2];
            chksdt = res2 + " "+res[1]+ " "+res[2];
        }
    if(chkedt !="")
        { 
            if(chkedt.match(reg))
            {
                var cres = chkedt.split(" ");
                var cres1 = cres[0].split("/");
                var cres2 = cres1[1]+"/"+cres1[0]+"/"+cres1[2];
                chkedt = cres2 + " "+cres[1]+ " "+cres[2];
            }
        }
        var  mchksdt  =  new Date(chksdt);
        var  mchkedt  =  new Date(chkedt);
       
        if($("#start_date_time").val() == "")
        {
            $("#start_date_time").addClass( "cnpcform-invalid" );
            $("#start_date_time").focus();
            return false;
        }
         if(chksdt != "" && chkedt != ""){
            if(mchksdt.getTime() > mchkedt.getTime())
            {
                $("#end_date_time").addClass( "cnpcform-invalid" );
                $("#end_date_time").focus();
                return false;
            }
        }
        
        var camp = cnpcChnlListValidate();
        var sdate = cnpcchnlStartDateValidate();
        var edate = cnpcchnlEndDateValidate();
        var nofrms = $("#cnpcchnl_counter").val();
        nofrms = nofrms-1;
        for(var j=0;j<=nofrms;j++)
        {
           
            var  strtdt1 = $("#chnlstartdate_"+j).val();
            var  chkedt1 = $("#chnlenddate_"+j).val();
           
            var ereg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
            if (strtdt1.match(ereg))
            {
                var eres = strtdt1.split(" ");
                var eres1 = eres[0].split("/");
                var eres2 = eres1[1]+"/"+eres1[0]+"/"+eres1[2];
                strtdt1 = eres2 + " "+eres[1]+ " "+eres[2];
            }
            if(chkedt1 !="")
            {
                if (chkedt1.match(ereg))
                {
                    var ecres  = chkedt1.split(" ");
                    var ecres1 = ecres[0].split("/");
                    var ecres2 = ecres1[1]+"/"+ecres1[0]+"/"+ecres1[2];
                    chkedt1    = ecres2 + " "+ecres[1]+ " "+ecres[2];
                }
            }
            var  mchksdt1  =  new Date(strtdt1);
            var  mchkedt1  =  new Date(chkedt1);
            if(strtdt1 != "" && chkedt1 == ""){
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    
                    /*alert("Channel start date should be greater or equal to group start date")
                    $("#chnlstartdate_"+j).focus();
                    return false;*/
                    $("#chnlstartdate_"+j).next().remove();
                    $("#chnlstartdate_"+j).after("<p class='cnpc-danger'>Channel start date should be greater or equal to group start date</p>");
                    $("#chnlstartdate_"+j).focus();
                    return false;
                }
                if(mchksdt1.getTime() > meddt.getTime() )
                {
                    /*alert("Channel start date should be less than or equal to group end date");
                    $("#chnlstartdate_"+j).focus();
                    return false;*/
                    $("#chnlstartdate_"+j).next().remove();
                    $("#chnlstartdate_"+j).after("<p class='cnpc-danger'>Channel start date should be less than or equal to group end date</p>");
                    $("#chnlstartdate_"+j).focus();
                    return false;
                }
            }    
            if(strtdt1 != "" && chkedt1 != ""){
                if(mchkedt1.getTime() > meddt.getTime() )
                {
                    /*alert("Channel end date should be less than or equal to group end date");
                    $("#chnlstartdate_"+j).focus();
                    return false;*/
                    $("#chnlstartdate_"+j).next().remove();
                    $("#chnlstartdate_"+j).after("<p class='cnpc-danger'>Channel end date should be less than or equal to group end date</p>");
                    $("#chnlstartdate_"+j).focus();
                    return false;
                }
                if(mchksdt1.getTime() < mstdt.getTime() )
                {
                    /*alert("Channel start date should be greater or equal to group start date");
                    $("#chnlstartdate_"+j).focus();
                    return false;*/
                    $("#chnlstartdate_"+j).next().remove();
                    $("#chnlstartdate_"+j).after("<p class='cnpc-danger'>Channel start date should be greater or equal to group start date</p>");
                    $("#chnlstartdate_"+j).focus();
                    return false;
                }
                if(mchksdt1.getTime() > mchkedt1.getTime())
                {       
                    /*alert("End date should be greater than or equal to start date");
                    $("#chnlenddate_"+j).focus();
                    return false;*/
                    $("#chnlenddate_"+j).next().remove();
                    $("#chnlenddate_"+j).after("<p class='cnpc-danger'>End date should be greater than or equal to start date</p>");
                    $("#chnlenddate_"+j).focus();
                    return false;
                }
            }       
        }
        
        if (camp  && sdate && edate)
        {
            var form_data = new FormData();
            var cnpcfgn = $("#cnpc_channel_group_name").val();
            var cnpcsdt = $("#start_date_time").val();
            var cnpcedt = $("#end_date_time").val();
            var cmpcmsg = $("#cnpc_message").val();
            var status = $("#cnpc_status").val();
            var formAction = $("#cnpc_action").val();
             var cnpcacc = $("#cnpc_accounts").val();
            form_data.append('cnpc_action', formAction);
            form_data.append('cnpc_group_name', cnpcfgn);
            form_data.append('cnpc_start_datetime', cnpcsdt);
            form_data.append('cnpc_end_datetime', cnpcedt);
             form_data.append('cnpc_account', cnpcacc);
            form_data.append('cnpc_message', cmpcmsg);
            form_data.append('cnpc_status', status);
            
            var basePath = $("#base_url_cnpc").val();
            var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/savechanneldata/";
           
            if(formAction == "add")
            {
                var camp_list = getValues("cnpc_chnl_list[]");;
                var startdate = getValues("chnlstartdate[]");
                var enddate = getValues("chnlenddate[]");
            }
            else
            {
                var camp_list = getFormSelectValues("cnpc_chnl_list");
                 var startdate = getFormValues("chnlstartdate");
                var enddate = getFormValues("chnlenddate");
                var cnpc_edit_id = $("#cnpc_edit_id").val();
                form_data.append("cnpc_edit_id",cnpc_edit_id);
            }
            form_data.append("cnpc_chnl_list",camp_list);
            form_data.append("chnlstartdate",startdate);
            form_data.append("chnlenddate",enddate);
        $.ajax({
                url:url,
                data:form_data,
                cache : false,
                contentType: false,
                method:'POST',
                processData : false,
                dataType    : 'json',
                success:function(res){ 
                   console.log(res.data);
                    if(res.data == "success")
                    {
                        if(formAction=="edit")
                        {
                            window.location="../CnPpledgeTVChannels";
                        }
                        else
                        {
                           window.location = "CnPpledgeTVChannels";
                        }
                    }
                    //updateForms(form_gdata,res.data);
                    return false;
                },
            error:function(res){  console.log(res.data); console.log("error insert");
            },
            });
            
        }
     return false;
    });
     $(".deletechannelInfo").click(function(e){
        e.preventDefault();
        var did = $(this).data("recid");
         var basePath = $("#base_url_cnpc").val();
        var url = location.protocol + "//" + location.host + basePath + "admin/click_pledge_connect/deletechannel/"+did;
           
        $.ajax({
            url:url,
            type: "GET",
            success:function(res){
                if(res.data=="success")
                {
                    window.location="CnPpledgeTVChannels";
                }
                else
                {
                    window.location="CnPpledgeTVChannels";
                }
            },
            error:function(err){
                   console.log(err);
            },
        });
    });
    
function generateChannelTable(rowIndex)
    {
        //console.log(==1);


        var bPath = $("#base_url_cnpc").val();
        var trow = "";
        trow += "<tr id='row_" + rowIndex + "'>";
        
        trow += "<td>";
        trow += "<select  class='cnpc_chnl_list' data-rowindex='" + rowIndex + "'  name='cnpc_chnl_list[]' id='cnpc_chnl_list_" + rowIndex + "'>";
        trow += "<option value=''>Select Channel</option>";
        trow += "</select><img id='imgloader_" + rowIndex + "' src='" + bPath + "modules/click_pledge_connect/images/loading.gif' height='25' width='25'>";
        trow += "</td>";


        trow += "<td>";
        trow += "<input type='text' name='chnlstartdate[]' class='cnpcstartdate' id='chnlstartdate_" + rowIndex + "'>";
        trow += "</td>";


        trow += "<td>";
        trow += "<input type='text' name='chnlenddate[]' class='cnpcenddate' id='chnlenddate_" + rowIndex + "'>";
        trow += "</td>";


        trow += "<td id='row_" + rowIndex + "'>";
        trow += "<a href='javascript:void(0)' id='deleteRec_" + rowIndex + "' data-target='add' class='deleteRec' data-index='" + rowIndex + "'><img src='" + bPath + "modules/click_pledge_connect/images/cnpc-trash.png' height='20' width='20'></a>";
        trow += "</td>";
        trow += "</tr>";
        return trow;
    }


     $("#chnlsavebtn").click(function () {
         var  stdt  = $("#start_date_time").val();
           var  eddt  = $("#end_date_time").val();
           
          var greg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
          if (stdt.match(greg))
          {
             var gres  = stdt.split(" ");
             var gres1 = gres[0].split("/");
             var gres2 = gres1[1]+"/"+gres1[0]+"/"+gres1[2];
             stdt = gres2 + " "+gres[1]+ " "+gres[2];


         }
      if(eddt !="")
      {
          if (eddt.match(greg))
          {
             var gcres  = eddt.split(" ");
             var gcres1 = gcres[0].split("/");
             var gcres2 = gcres1[1]+"/"+gcres1[0]+"/"+gcres1[2];
             eddt       = gcres2 + " "+gcres[1]+ " "+gcres[2];


         }
     }
  var  mstdt = new Date(stdt);
  var  meddt = new Date(eddt);
 
       
        if ($("#cnpc_channel_group_name").val() == "")
        {
            $("#cnpc_channel_group_name").css("border", "1px solid red");
            $("#cnpc_channel_group_name").focus();
            return false;
        }
        else
        {
            $("#cnpc_channel_group_name").css("border", "1px"); 
        }
        if($("#start_date_time").val() == "")
        {
            $("#start_date_time").css("border", "1px solid red");
            $("#start_date_time").focus();
            return false;
        }
 
 if(mstdt.getTime() > meddt.getTime())
     {
         $("#start_date_time").css("border", "1px solid red");
            $("#start_date_time").focus();
        return false;
     }


         $(".cnpc_settings_form").show();
         //$("#imgloader_"+$("#cnpcchnl_counter").val()).show();

        getDataChannelService($("#cnpcchnl_counter").val());
        var tab = generateChannelTable($("#cnpcchnl_counter").val());
     
 $("tbody#dynachnlrows").append(tab);
        $(".cnpcstartdate").each(function () {
            $(this).datetimepicker({
                timeFormat: 'hh:mm tt',
                dateFormat: 'MM dd, yy',
            });
        });


        $(".cnpcenddate").each(function () {
            $(this).datetimepicker({
                timeFormat: 'hh:mm tt',
                dateFormat: 'MM dd, yy',
            });
            
        });
        //$("#custrows").after('<a href="javascript:void(0)" onclick="addrows()" id="addrows">Add Form</a>');
        $("#chnlsavebtn").hide();
        $("#resetbtn").hide();
        $(".cnpc_hide_show_buttons").show();
        $("#cnpcchnl_counter").val(1);



        return false;
            
    });

   $(".cnpc_question").mouseover(function(){
       $(".cnpc_shortcode_tooltip").show();
   });
   $(".cnpc_question").mouseout(function(){
       $(".cnpc_shortcode_tooltip").hide();
   });
   
   
   $(".cnpc_shortcode_tooltip").mouseover(function(){
       $(this).show();
   });
   $(".cnpc_shortcode_tooltip").mouseout(function(){
       $(this).hide();
   });
   function validateBasicForm()
   {
       var status = true;
       if ($("#cnpc_form_group_name").val() == "")
        {
            $("#cnpc_form_group_name").css("border", "1px solid red");
            $("#cnpc_form_group_name").focus();
            status = false;
        }
         var  stdt  = $("#start_date_time").val();
        var  eddt  = $("#end_date_time").val();
        var greg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        if (stdt.match(greg))
        {
            var gres  = stdt.split(" ");
            var gres1 = gres[0].split("/");
            var gres2 = gres1[1]+"/"+gres1[0]+"/"+gres1[2];
            stdt = gres2 + " "+gres[1]+ " "+gres[2];
        }
        if(eddt !="")
        {
            if (eddt.match(greg))
            {
                var gcres  = eddt.split(" ");
                var gcres1 = gcres[0].split("/");
                var gcres2 = gcres1[1]+"/"+gcres1[0]+"/"+gcres1[2];
                eddt       = gcres2 + " "+gcres[1]+ " "+gcres[2];
           }
        }
       
        var  mstdt = new Date(stdt);
        var  meddt = new Date(eddt);
        /*****************************************/
        var  campfldnm;
        var  frmfldnm;
        var  guidfldnm;
        var  chksdt   = $("#start_date_time").val();
        var  chkedt   = $("#end_date_time").val()
  
        var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        if (chksdt.match(reg))
        {
            var res = chksdt.split(" ");
            var res1 = res[0].split("/");
            var res2 = res1[1]+"/"+res1[0]+"/"+res1[2];
            chksdt = res2 + " "+res[1]+ " "+res[2];
        }
       
	if(chkedt !="")
        { 
            if(chkedt.match(reg))
            {
                var cres = chkedt.split(" ");
                var cres1 = cres[0].split("/");
                var cres2 = cres1[1]+"/"+cres1[0]+"/"+cres1[2];
                chkedt = cres2 + " "+cres[1]+ " "+cres[2];
            }
            
        }
        var  mchksdt  =  new Date(chksdt);
        var  mchkedt  =  new Date(chkedt);
       
        if($("#start_date_time").val() == "")
        {
            $("#start_date_time").addClass( "cnpcform-invalid" );
            $("#start_date_time").focus();
            status = false;
        }
         if(chksdt != "" && chkedt != ""){
            if(mchksdt.getTime() > mchkedt.getTime())
            {
                $("#end_date_time").addClass( "cnpcform-invalid" );
                $("#end_date_time").focus();
                status = false;
            }
        }
        //display type validation
        if($("#cnpc_display_type").val() == "")
        {
            $("#cnpc_display_type").addClass( "cnpcform-invalid" );
            $("#cnpc_display_type").focus();
            status = false;
        }
        if($("#cnpc_display_type").val() == "popup" && $("#cnpc_link_type").val() == "")
        {
            $("#cnpc_link_type" ).addClass( "cnpcform-invalid" );
            status = false;
        }
         if($("#cnpc_display_type").val() == "popup" && ($("#cnpc_display_type").val() == "text" || $("#cnpc_display_type").val() == "button") && $("#cnpc_btn_lbl").val() == "")
        {
            $("#cnpc_btn_lbl" ).addClass( "cnpcform-invalid" );
            status = false;
        }
        
            var cnpcdtype = $("#cnpc_display_type").val();            
         
            
            if(cnpcdtype=="popup")
            {
                var cnpcltype = $("#cnpc_link_type").val();
                
                if(cnpcltype=="image")
                {
                    var filepath= $("#cnpc_upload_image").val();
                    //if file uploads, move the file into server location
                    var files = document.getElementById('cnpc_upload_image').files;
                    if(files.length!=0)
                    {
                        if(files[0].type=="image/png" || files[0].type=="image/jpg" || files[0].type=="image/jpeg")
                        {
                            $("#cnpc_upload_image").css("border","1px solid rgb(184, 184, 184)");
                        }
                        else
                        {
                            //alert("Please select a valid image(.png, .jpg)");
                             $("#cnpc_upload_image").css("border", "1px solid red");
                            $("#cnpc_upload_image").focus();
                            status = false;
                        }
                    }
                    else
                    {
                        if($("#cnpc_action").val()=="edit")
                        {
                            var hiddenImg = $("#cnpc_upload_image_hidden").val();
                            
                        }
                        else
                        {
                            //alert("Please select an image");
                             $("#cnpc_upload_image").css("border", "1px solid red");
                            $("#cnpc_upload_image").focus();
                            status = false;
                        }
                    }
                }
                else
                {
                    var btnlbl = $("#cnpc_btn_lbl").val();
                    var linktype = $("#cnpc_link_type").val();
                    
                    
                    if(btnlbl != "")
                    {
                       $("#cnpc_btn_lbl").css("border","1px solid rgb(184, 184, 184)");
                    }
                    else
                    {
                        //alert("Please enter button label");
                         $("#cnpc_btn_lbl").css("border", "1px solid red");
                        $("#cnpc_btn_lbl").focus();
                        status = false;
                    }
                }
            }
            return status;
   }
    
})(jQuery, Drupal);