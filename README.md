CONTENTS OF THIS FILE
---------------------
* Introduction
* Installation
* Configuration

INTRODUCTION
------------
This module will display Click & Pledge connect forms

* For a full description of the module, visit the project page:
  
* To submit bug reports and feature suggestions, or to track changes:
  https://forums.clickandpledge.com/forum/platform-product-forums/3rd-party-integrations/drupal-commerce

INSTALLATION
------------
* download the module from below link,
http://drupal.org/node/1897420

CONFIGURATION
-------------
* Please visit the below link to configuration settings